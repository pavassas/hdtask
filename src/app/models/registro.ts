export interface Registro{
    id : string,
    idvehiculo:string,
    fecha : string
    local:string,
    categoria:string,
    nota : string,
    detalle: Array<string>,
    open:boolean,
    km:number
}

export interface Categoria{
    id : string,
    nombre:string,
}

export interface Local{
    id : string,
    nombre:string,
}

export interface Detalle {
    cat: string;
    ck:boolean,
    nombre:string,
    tipo: string;
    valor: number;

  }