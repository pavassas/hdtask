import { Component, OnInit,PipeTransform } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  LoadingController,
  PopoverController,
  ModalController } from '@ionic/angular';

import { DecimalPipe } from '@angular/common';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
// Modals
import { SearchFilterPage } from '../../componentes/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
//import { NotificationsComponent } from './../../componentes/notifications/notifications.component';

import { ModalNuevoComponent  } from '../../modals/modal-nuevo/modal-nuevo.component';

import { ModalSolucionarComponent  } from '../../modals/modal-solucionar/modal-solucionar.component';

import { ModalRecordatorioComponent  } from '../../modals/modal-recordatorio/modal-recordatorio.component';
import { ModalDetalleComponent  } from '../../modals/modal-detalle/modal-detalle.component';

// servicios
import { AuthService } from '../../servicios/auth.service';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
//import { RegistroService } from '../../servicios/registro.service';
//import { VehiculoService } from '../../servicios/vehiculo.service';
import { LocalNotifications, ILocalNotificationActionType } from '@ionic-native/local-notifications/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Task } from '../../interfaces/task';

import { NgbModalOptions, NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
//import { EmailComposer } from '@ionic-native/email-composer/ngx';
const TASK: Task[] = [];

//find node_modules -name "*.mjs" | xargs rm cuando fallen esos archivo y borrar www y platform

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss'],
})
export class HomeResultsPage {
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/icon/icon.png';
  information: any[];
  automaticClose = false;
  public registroList: any = [];
  public registroList2: any = [];
  cate: any;
  local: any;
  mivehiculo: any;
  schedule: any;
  
  //public vehiculoList: any = [];
  public alert1: any = 0;//nueva tarea
  public alert2: any = 0;//actualizaron tarea que asigno
  public alert3: any = 0;//completaron tarea que asigno
  public alert4: any = 0;//nuevo recordatorio
  clickSub: any;
  
  formNuevo: FormGroup;
  formSolucion: FormGroup;

  start: any=0;
  lim: any =10;
  miusuario:any;
  searchQuery:any;
  public usuarioList: any = [];
  //public listatareas$: Task[] = [];

  public listatareas$: Observable<Task[]>;
  filter = new FormControl('');

  contador;
  resultado;
  pendientes;
  solicitadas;
  cerradas;
  closeResult = '';
  options : NgbModalOptions = {
    windowClass: 'myCustomModalClass',
    keyboard: false,
    backdrop: 'static',
    backdropClass: 'light-blue-backdrop',
    centered: true,
    size: 'lg',
    ariaLabelledBy:'modal-basic-title'
  }
  
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private localNotifications: LocalNotifications,
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    //public registroservice: RegistroService,
    //public vehiculoservice: VehiculoService,
    private AFauth: AngularFireAuth,
    private modalService: NgbModal,
    //private emailComposer: EmailComposer
  ) {
    
  }
  

  validation_messages = {
    'Detalle': [
      { type: 'required', message: 'Requerido' }
    ],
    'Asignar': [
      { type: 'required', message: 'Requerido' }
    ],
    'Solucion': [
      { type: 'required', message: 'Requerido' }
    ]
  };

 
  
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {

    //this.correo(); 
    

    this.authservice.getUsuarios().subscribe( (usu) => {
      this.usuarioList = usu;
      //this.usuarioAsig = usu
    });
    this.resetContador();
    this.listTareas('Pendientes');

    /*this.listatareas$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text, this.pipe))
    );*/
    // asyncScheduler.schedule(this.task, 1000, 0);
    //this.alertNotificacion();
    // Schedule delayed notification
    return new Promise<any>((resolve, rejected) => {

      this.authservice.count_recordatorio('tareanueva').subscribe( record => {
        
        let cant = record.length;
        console.log("Cantidad de tarea:"  + cant);
        if(cant>parseInt(this.alert1))
        {
          console.log("Aumento en tarea");
          this.alert1 = record.length;
          this.resetContador();
          this.listTareas('Pendientes');
          this.lanzarRecordatorio("Se le asigno una nueva tarea");
        }
        else
        {
          console.log("No aumento en tarea");
        }
      }) 
      
      this.authservice.count_recordatorio('tareaactualizada').subscribe( record => {
        
        let cant = record.length;
        console.log("Cantidad de tarea:"  + cant);
        if(cant>parseInt(this.alert2))
        {
          console.log("Aumento en tarea");
          this.alert2 = record.length;
          //this.resetContador();
          this.listTareas('Solicitadas');
          this.lanzarRecordatorio("Se le actualizo una tarea");
        }
        else
        {
          console.log("No aumento en tarea");
        }
      })  

      this.authservice.count_recordatorio('tareacompletada').subscribe( record => {
        
        let cant = record.length;
        console.log("Cantidad de tarea:"  + cant);
        if(cant>parseInt(this.alert3))
        {
          console.log("Aumento en tarea");
          this.alert3 = record.length;
          this.resetContador();
          this.listTareas('3');
          this.lanzarRecordatorio("Se le completo una tarea");
        }
        else
        {
          console.log("No aumento en tarea");
        }
      })  
      this.authservice.count_recordatorio('tarearecordatorio').subscribe( record => {
        
        let cant = record.length;
        console.log("Cantidad de tarea:"  + cant);
        if(cant>parseInt(this.alert3))
        {
          console.log("Aumento en tarea");
          this.alert3 = record.length;
          //this.resetContador();
          this.listTareas('Pendientes');
          this.lanzarRecordatorio("Tiene un nuevo recordatorio de tarea pendientes");
        }
        else
        {
          console.log("No aumento en tarea");
        }
      })  
      
    //this.resetNuevo();
    });
  }
  
  search(text: string, pipe: PipeTransform): Task[] {
    return TASK.filter(task => {
      const term = text.toLowerCase();
      return task.Detalle.toLowerCase().includes(term)
          || pipe.transform(task.Solucion).includes(term)
          || pipe.transform(task.Id).includes(term);
    });
  }
  resetContador()
  {
    this.authservice.getContador(localStorage.getItem('idusuario')).subscribe((data)=>{
      this.contador = JSON.parse(JSON.stringify(data));
      this.pendientes = this.contador.pendientes;
      this.solicitadas = this.contador.solicitadas;
      this.cerradas = this.contador.cerradas;
    },(error)=>{
      console.log(error);
    })
  }
  async deleteTask(id: string, index: number) {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Realmente desea eliminar la tarea seleccionada?',
     // message: 'Message <strong>text</strong>!!!',
      buttons: [
         {
          text: 'Aceptar',
          cssClass: 'btn btn-primary',
          handler: () => {
            this.authservice.deleteTask(id,localStorage.getItem('idusuario'))
            .subscribe((data) => {

              this.resultado = JSON.parse(JSON.stringify(data));
              if(this.resultado.res)
              {
                this.authservice.error2(this.resultado.msn);
              }
              else
              {
                //this.listatareas$.splice(index, 1);
                this.authservice.ok2(this.resultado.msn);
              }     
            });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn btn-danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  async execTask(id: string, index: number) {
    let task = {
      Id:id,
      Detalle: "",      
      Asignar: "",
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   "",
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'EJECUTARTAREA',
      color: '#FFFFFF'
    };
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Realmente desea completar la tarea seleccionada ?',
      //message: 'Message <strong>text</strong>!!!',
      buttons: [
         {
          text: 'Aceptar',
          cssClass: 'btn btn-primary',
          handler: () => {
            this.authservice.executeTask(task)
            .subscribe((data) => {

              this.resultado = JSON.parse(JSON.stringify(data));
              if(this.resultado.res)
              {
                this.authservice.error2(this.resultado.msn);
              }
              else
              {
                //this.listatareas$.splice(index, 1);
                this.authservice.ok2(this.resultado.msn);
                let idtarea = this.resultado.idtarea;
                let para =  this.resultado.para;
                let asi =  this.resultado.asignada;
                const datos = {
                  asignada:asi,
                  para: para,
                  mensaje: "Se cerro tarea N°" + idtarea,
                  estado: "0",
                  tipo: "3",
                  idtarea:idtarea
                }
                if(localStorage.getItem('idusuario')!=asi)
                {
                  this.authservice.create_recordatorioTask(datos)
                  .then(
                    () => {
                      this.authservice.ok2("Se registro recordatorio al cierre de tarea");
                    }).catch(err => {
                        this.authservice.error2(err);
                    });
                }
              }     
            });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn btn-danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  async rechazarTask(id: string, index: number)
  {
      // tslint:disable-next-line: prefer-const
      let mensage: string;
      const alert = await this.alertCtrl.create({
        header: 'Rechazo tarea',
        message: 'Digite el motivo por el cual se rechaza la tarea',
        inputs: [
          { name: 'motivo', type: 'text', placeholder: 'Motivo'},
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'btn btn-danger',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Confirmar',
            cssClass: 'btn btn-primary',
            handler: async (alertData) => {
              // tslint:disable-next-line: max-line-length
              const loader = await this.loadingCtrl.create({
                duration: 2000
              });
              loader.present();              
              // tslint:disable-next-line: triple-equals
              if (alertData.motivo == '' || alertData.motivo == null || alertData.motivo == undefined){
                mensage = 'Ingrese un motivo de rechazo';
              } 
              else 
              {
                  let mot = this.authservice.nl2br(alertData.motivo);
                  console.log('si');
                  let task = {
                    Id:id,
                    Detalle: "",      
                    Asignar: "",
                    Estado:0,
                    Creacion: "",
                    Cierre:"",
                    Asignada: "",
                    Para:"",
                    Numreco:0,
                    Env:"",
                    Acciones:true,
                    Numsol:     0,
                    Solucion: mot  ,
                    Eli:        false,
                    Fer:        false,
                    idusuario: localStorage.getItem('idusuario'),
                    opcion: 'RECHAZARTAREA',
                    color: '#FFFFFF'
                  };
                   // tslint:disable-next-line: only-arrow-functions
                   this.authservice.executeTask(task)
                   .subscribe((data) => {
       
                     this.resultado = JSON.parse(JSON.stringify(data));
                     if(this.resultado.res)
                     {
                      mensage = this.resultado.msn;
                       this.authservice.error2(this.resultado.msn);
                     }
                     else
                     {
                       //this.listatareas$.splice(index, 1);
                       this.authservice.ok2(this.resultado.msn);
                       mensage = this.resultado.msn;
                       let idtarea = this.resultado.idtarea;
                       let para =  this.resultado.para;
                       let asi =  this.resultado.asignada;
                       const datos = {
                         asignada:asi,
                         para: para,
                         mensaje: "Se rechazo tarea N°" + idtarea,
                         estado: "0",
                         tipo: "5",
                         idtarea:idtarea
                       }
                       if(localStorage.getItem('idusuario')!=asi)
                       {
                         this.authservice.create_recordatorioTask(datos)
                         .then(
                           () => {
                             this.authservice.ok2("Se registro recordatorio al cierre de tarea");
                           }).catch(err => {
                               this.authservice.error2(err);
                           });
                       }
                     }     
                   });
                }
              
              loader.onWillDismiss().then(async l => {
                const toast = await this.toastCtrl.create({
                  showCloseButton: true,
                  message: mensage,
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              });
            }
          }
        ]
      });
  
      await alert.present();
    
  }

  async archTask(id: string, index: number) {
    let task = {
      Id:id,
      Detalle: "",      
      Asignar: "",
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   "",
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'APROBARTAREA',
      color: '#FFFFFF'
    };
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Realmente desea archivar la tarea seleccionada ?',
      //message: 'Message <strong>text</strong>!!!',
      buttons: [
         {
          text: 'Aceptar',
          cssClass: 'btn btn-primary',
          handler: () => {
            this.authservice.executeTask(task)
            .subscribe((data) => {

              this.resultado = JSON.parse(JSON.stringify(data));
              if(this.resultado.res)
              {
                this.authservice.error2(this.resultado.msn);
              }
              else
              {
                //this.listatareas$.splice(index, 1);
                this.authservice.ok2(this.resultado.msn);
              }     
            });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn btn-danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  openNuevo() {
       this.modalService.open(ModalNuevoComponent,this.options).result.then((result) => {
        console.log( `Closed with: ${result}`);
      }, (reason) => {
        this.resetContador();
        this.listTareas('Pendientes');
        console.log( `Dismissed ${this.getDismissReason(reason)}`);
      });    
  }
  openSolucionar(IdTask:string, index: number) {
    localStorage.setItem('IdTask',IdTask);
    this.modalService.open(ModalSolucionarComponent,this.options).result.then((result) => {
     console.log( `Closed with: ${result}`);
     this.listTareas('');
   }, (reason) => {
     this.resetContador();
     //this.listTareas('Pendientes');
     //this.listatareas$.splice(index, 1);
     this.listTareas('3');
     console.log( `Dismissed ${this.getDismissReason(reason)}`);
   });    
}
openRecordatorio(IdTask:string, index: number) {
  localStorage.setItem('IdTask',IdTask);
  this.modalService.open(ModalRecordatorioComponent,this.options).result.then((result) => {
   console.log( `Closed with: ${result}`);
 }, (reason) => {
   this.resetContador();
   //this.listTareas('Pendientes');
   console.log( `Dismissed ${this.getDismissReason(reason)}`);
 });    
}

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  changeListaTareas() {
    localStorage.setItem('searhQuery',this.searchQuery)
    localStorage.setItem('idususel', this.miusuario);
    this.authservice.idususel = this.miusuario;
    this.listTareas(localStorage.getItem('est'));
  }
  async presentAlert(data) {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }
  listTareas(est:string)
  { 
    if(est==""){ est =  localStorage.getItem('est'); }
    localStorage.setItem('est',est);
    this.authservice.getTareas(est,localStorage.getItem('idusuario'),this.miusuario,this.start,this.lim,this.searchQuery).subscribe( (data:any[])=>{
      this.listatareas$ = JSON.parse(JSON.stringify(data));
      //TASK = JSON.parse(JSON.stringify(data));
      console.log(this.listatareas$);
    },(error)=>{
      console.log(error);
    })
  }
  lanzarRecordatorio(msn:string)
  {
    this.localNotifications.schedule({
      title: 'Alerta HDTASK',
      text: msn,
      trigger: { at: new Date(new Date().getTime() + 100) },
      led: 'FF0000',
      vibrate: true,
      foreground: true,
      //sound: null
    });
  }


  

  resetHome() {
    this.mivehiculo = localStorage.getItem('idVehiculo');
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async searchFilter() {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async notifications(op:string,id:string="") {
    localStorage.setItem("opDetalle",op);
    localStorage.setItem("IdTask",id);
    
    /*let ev: any;
   
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true,
      translucent: true
    });
    return await popover.present();*/

    this.modalService.open(ModalDetalleComponent,this.options).result.then((result) => {
      console.log( `Closed with: ${result}`);
    }, (reason) => {
      this.resetContador();
      //this.listTareas('Pendientes');
      //this.listatareas$.splice(index, 1);
      console.log( `Dismissed ${this.getDismissReason(reason)}`);
    });    
  }
  unsub() {
    this.clickSub.unsubscribe();
  }

  // simple notificacion
  simpleNotif() {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' }
    });

  }
  // generate
  multipleNotif() {
    this.localNotifications.schedule([{
      id: 1,
      text: 'Multi ILocalNotification 1',
      data: { secret: 'data' }
    }, {
      id: 2,
      title: 'Local LocalNotification Example',
      text: 'Multi ILocalNotification 2',
      icon: 'assets/ionic_white.png'
    }]);
  }

  
  delayedNotif() {
    this.localNotifications.schedule({
      text: 'Delayed ILocalNotification',
      trigger: { at: new Date(new Date().getTime() + 5000) },
      led: 'FF0000',
      sound: null
    });
  }

  progressNotif() {
    this.localNotifications.schedule({
      title: 'Sync in progress',
      text: 'Copied 2 of 10 files',
      progressBar: { value: 20 }
    });
  }

  multiLineNotif() {
    this.localNotifications.schedule({
      title: 'The Big Meeting',
      text: '4:15 - 5:15 PM\nBig Conference Room',
      smallIcon: 'res://calendar',
      icon: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzfXKe6Yfjr6rCtR6cMPJB8CqMAYWECDtDqH-eMnerHHuXv9egrw'
    });
  }

  summaryNotif() {
    this.localNotifications.schedule({
      title: 'Chat with Irish',
      icon: 'https://enappd.com/static/images/enappd-logo-blue.png',
      text: ['I miss you', 'Irish : I miss you more!', 'I always miss you more by 10%']
    });
  }

  actionNotif() {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      title: 'The big survey',
      text: 'Are you a fan of RB Leipzig?',
      attachments: ['http://placekitten.com/g/300/200'],
      actions: [
        { id: 'yes', title: 'Yes' },
        { id: 'no', title: 'No' }
      ]
    });
  }

  inputNotif() {
    this.localNotifications.schedule({
      title: 'Justin Rhyss',
      text: 'Do you want to go see a movie tonight?',
      actions: [{
        id: 'reply',
        type: ILocalNotificationActionType.INPUT,
        title: 'Reply'
      }]
    });
  }

  groupNotif() {
    this.localNotifications.schedule([
      { id: 0, title: 'Design team meeting' },
      { id: 1, summary: 'me@gmail.com', group: 'email', groupSummary: true },
      { id: 2, title: 'Please take all my money', group: 'email' },
      { id: 3, title: 'A question regarding this plugin', group: 'email' },
      { id: 4, title: 'Wellcome back home', group: 'email' }
    ]);
  }
  /////////////
  foreNotif() {
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' },
      foreground: true
    });
  }
  task(state) {
    console.log(state);
    this.schedule(state + 1, 1000); // `this` references currently executing Action,
     // which we reschedule with new state and delay
    console.log('Prueba de carga');
    console.log('Prueba 2');
  }

  alertNotificacion() {
    this.localNotifications.schedule({
      text: 'si estoy notificando',
      trigger: {at: new Date(new Date().getTime() + 3600)}, 
      led: 'FF0000',
      sound: null
    });
  }

  alertapruebatiempo() {
    this.localNotifications.schedule({
      title: 'Design team meeting',
      text: '3:00 - 4:00 PM',
      trigger: { at: new Date(2019, 9, 26, 16) }
  });
  }

  RUTA(ruta) {
    this.navCtrl.navigateForward(ruta);
  }
}
