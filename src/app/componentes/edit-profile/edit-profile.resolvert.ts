import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';


@Injectable()
export class PerfilResolver implements Resolve<any> {

  constructor(
    private mePerfil: AuthService ) {}
    resolve(route: ActivatedRouteSnapshot) {
        return new Promise((resolve, reject) => {
            let itemId = localStorage.getItem('idUsuario');
            this.mePerfil.edit_perfil(itemId)
            .then(data => {
            data.id = itemId;
            resolve(data);
            }, err => {
            reject(err);
            });
        });
    }
}
