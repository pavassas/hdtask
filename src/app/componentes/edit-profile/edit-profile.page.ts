import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { AuthService } from 'src/app/servicios/auth.service';
import {  Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  formperfil: FormGroup;
  item: any;
  image: any;
  public upd:boolean;
  public nombreCompleto: string;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private webview: WebView,
    private crop: Crop,
    private camera: Camera,
    private file: File,
    public actionSheetController: ActionSheetController,
    public auth : AuthService

    ) { }
    validation_messages = {
      'Nombre': [
        { type: 'required', message: 'El nombre es requerido' }
      ],
      'Apellido': [
        { type: 'required', message: 'El apellido es requerido' }
      ],
    };

  ngOnInit() {
    this.nombreCompleto = localStorage.getItem('nomuser');
 
    this.getData();
  }


  getData() {
    this.route.data.subscribe(routeData => {
     let data = routeData['data'];
     if (data) {
       this.item = data;
       this.image = this.item.imagen;
       console.log(this.item.imagen);
     }
    });
    this.formperfil = this.formBuilder.group({
      Nombre: new FormControl(this.item.nombre, Validators.required),
      Apellido: new FormControl(this.item.apellido, Validators.required)
    });
  }
  async GUARDARPERFIL(values) {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });
    loader.present();
    let datos = {
      nombre: values.Nombre,
      apellido: values.Apellido,
      uid: localStorage.getItem('idUsuario'),
      idVehiculo: this.item.idVehiculo,
      imagen: this.item.imagen
    };
    console.log(datos);
    this.authservice.update_perfil( localStorage.getItem('idUsuario'), datos)
    .then(
      res => {
        //this.funciones.ok2('Perfil actualizado');
        this.nombreCompleto = values.Nombre + ' ' + values.Apellido;
        localStorage.setItem('nomuser', values.Nombre + ' ' + values.Apellido );
      }
    ).catch(err => {
      this.authservice.error2(err);
    });

  }
  pickImage(sourceType) {
    const options: CameraOptions = {
    quality: 100,
    sourceType: sourceType,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.cropImage(imageData)
      }, (err) => {
      // Handle error
      }
    
      );
    }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Seleccionar recurso",
      buttons: [{
        text: 'Mostrar galeria',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Usar Camara',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancelar',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  cropImage(fileUrl) {
  this.crop.crop(fileUrl, { quality: 50 })
    .then(
      newPath => {
        this.showCroppedImage(newPath.split('?')[0])
      },
      error => {
        this.auth.error2('Error cropping image' + error);
      }
    );
  }
  showCroppedImage(ImagePath) {
    //this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      //this.croppedImagepath = base64;//this.image
      //this.isLoading = false;
      this.uploadImageToFirebase(base64);

    }, error => {
      this.auth.error2('Error in showing image' + error);
      //this.isLoading = false;
    });
  }

  async uploadImageToFirebase(image){
    const loading = await this.loadingCtrl.create({
      message: 'Espere por favor...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada correctamente',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    /*this.auth.uploadImage(image_src, randomId)
    .then(photoURL => {
      this.image = photoURL;
      loading.dismiss();
      toast.present();
    }, err =>{
      console.log(err);
    })*/
    
    if(this.image!='./assets/img/imguser.png')
    {    
      this.auth.replaceImage(image_src, randomId,this.image)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
        this.updateImagen();
      }, err =>{
        this.upd = false
        console.log(err);
      })
    }
    else
    {
      this.auth.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
        this.updateImagen()
      }, err =>{
        console.log(err);
        this.upd = false;
      }) 
    }
    
  }
  updateImagen(){
    let datos = {
      nombre: this.item.nombre,
      apellido: this.item.apellido,
      uid: localStorage.getItem('idUsuario'),
      idVehiculo: this.item.idVehiculo,
      imagen: this.image
    };
    console.log(datos);
    this.authservice.update_perfil(localStorage.getItem('idUsuario'), datos)
    .then(
      res => {        
        console.info("Actualizo Perfil");
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }
  async presentLoading(loading) {
    return await loading.present();
  }
}
