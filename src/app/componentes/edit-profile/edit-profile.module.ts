import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EditProfilePage } from './edit-profile.page';
import { PerfilResolver } from './edit-profile.resolvert';
const routes: Routes = [
  {
    path: '',
    component: EditProfilePage,
    resolve: {
      data: PerfilResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditProfilePage],
  providers: [PerfilResolver]
})
export class EditProfilePageModule {}
