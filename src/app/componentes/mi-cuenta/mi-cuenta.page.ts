import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';


@Component({
  selector: 'page-mi-cuenta',
  templateUrl: 'mi-cuenta.page.html'
})
export class MiCuentaPage {
  imagenperfil:any;
  nomusuario:any;
  constructor(
    private authservice: AuthService,
    ) { 

    }
    ngOnInit() {
      this.imagenperfil = localStorage.getItem('imguser');
      this.nomusuario = localStorage.getItem('nomuser');
    }
}
