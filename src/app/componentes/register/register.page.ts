import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NavController, MenuController, LoadingController, ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { PasswordValidator } from '../validators/password.validator';
//import { ImageModalPage } from '../../componentes/image-modal/image-modal.page';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AuthService } from '../../servicios/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public onRegisterForm: FormGroup;
  public email: string;
  public name: string;
  public password: string;
  public apelli: string;
  sliderOpts = {
    zoom: false,
    slidesPerView: 1.5,
    spaceBetween: 20,
    centeredSlides: true
  };
 
  image: any;
  // tslint:disable-next-line: variable-name
  matching_passwords_group: FormGroup;
  public errorMessage: string = '';
  public successMessage: string = '';

  validation_messages = {
    /*'username': [
      { type: 'required', message: 'Username is required.' },
      { type: 'minlength', message: 'Username must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
      { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      { type: 'validUsername', message: 'Your username has already been taken.' }
    ],*/
    'name': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
    'lastname': [
      { type: 'required', message: 'El apellido es requerido' }
    ],
    'email': [
      { type: 'required', message: 'Email es requerido' },
      { type: 'pattern', message: 'Por favor ingresa un email valido' }
    ],
    /*'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'validCountryPhone', message: 'The phone is incorrect for the selected country.' }
    ],*/
    'password': [
      { type: 'required', message: 'Contraseña es requerida' },
      { type: 'minlength', message: 'La contraseña debe tener al menos 5 caracteres de longitud.' },
      { type: 'pattern', message: 'Su contraseña debe contener al menos  un número.' }// { type: 'pattern', message: 'Su contraseña debe contener al menos una mayúscula, una minúscula y un número.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirmación contraseña es requerida' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Contraseña no coincide.' }
    ],
    'terms': [
      { type: 'pattern', message: 'Debes ACEPTAR los términos y condiciones.' }
    ],
  };


  // croppedImagepath = "";
  // = false;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private webview: WebView,
    private crop: Crop,
    private camera: Camera,
    private file: File,
    public actionSheetController: ActionSheetController,
    private modalController: ModalController,
    private auth: AuthService,
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    /*this.onRegisterForm = this.formBuilder.group({
      'fullName': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });*/
    this.resetFields();
  }
  resetFields() {
    this.image = './assets/img/imguser.png';
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[0-9])[a-zA-Z0-9]+$')// '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$'
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });
    this.onRegisterForm = this.formBuilder.group({
      /*username: new FormControl('', Validators.compose([
        //UsernameValidator.validUsername,
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])),*/
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group,
      terms: new FormControl(true, Validators.pattern('true'))
    });
  }

  async signUp() {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    loader.present();
    loader.onWillDismiss().then(() => {
      this.navCtrl.navigateRoot('/vehiculo');
    });
  }

  // // //
  goToLogin() {
    this.navCtrl.navigateRoot('/login');
  }

  OnSubmitRegister(value) {
    this.auth.register(value.email, value.matching_passwords.password, value.name, value.lastname, this.image).then( auth => {
      localStorage.setItem('nomuser', value.name);
      localStorage.setItem('imguser', this.image);
      this.navCtrl.navigateRoot('/vehiculo');
      // value.fechanaci, value.cel, value.dire,
      console.log(auth);
    }).catch(
      err => this.auth.error2('Ya existe una cuenta con el correo electronico ingresado')
    );
  }


  pickImage(sourceType) {
    const options: CameraOptions = {
    quality: 100,
    sourceType: sourceType,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.cropImage(imageData); }, (err) => { } );
    }

    async selectImage() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Seleccionar recurso',
        buttons: [{
          text: 'Mostrar galeria',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar Camara',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }

    cropImage(fileUrl) {
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          this.auth.error2('Error cropping image' + error);
        }
      );
    }
    showCroppedImage(ImagePath) {
      // this.isLoading = true;
      // tslint:disable-next-line: prefer-const
      let copyPath = ImagePath;
      // tslint:disable-next-line: prefer-const
      let splitPath = copyPath.split('/');
      const imageName = splitPath[splitPath.length - 1];
      const filePath = ImagePath.split(imageName)[0];
      this.file.readAsDataURL(filePath, imageName).then(base64 => {
        // this.croppedImagepath = base64;//this.image
        // this.isLoading = false;
        this.uploadImageToFirebase(base64);

      }, error => {
        this.auth.error2('Error in showing image' + error);
        // this.isLoading = false;
      });
    }
  /*openImagePicker(){
    this.imagePicker.hasReadPermission()
    .then((result) => {
      if(result == false){
        // no callbacks required as this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
        this.imagePicker.getPictures({
          maximumImagesCount: 1
        }).then(
          (results) => {
            for (var i = 0; i < results.length; i++) {
              this.uploadImageToFirebase(results[i]);
            }
          }, (err) => console.log(err)
        );
      }
    }, (err) => {
      console.log(err);
    });
  }*/

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Espere por favor...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada∫∫ correctamente',
      duration: 3000
    });
    this.presentLoading(loading);
    // tslint:disable-next-line: variable-name
    const image_src = this.webview.convertFileSrc(image);
    const randomId = Math.random().toString(36).substr(2, 5);

    // uploads img to firebase storage
    this.auth.uploadImage(image_src, randomId)
    .then(photoURL => {
      this.image = photoURL;
      loading.dismiss();
      toast.present();
    }, err => {
      console.log(err);
    });
  }

  async presentLoading(loading) {
    return await loading.present();
  }
  openPreview(img) {
    this.modalController.create({
      component: "",//ImageModalPage,
      componentProps: {
        img: img
      }
    }).then(modal => {
      modal.present();
    });
  }
}
