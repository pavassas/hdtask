import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

import { AngularFireAuth } from "@angular/fire/auth";
import { isNullOrUndefined } from 'util';
import { AuthService } from 'src/app/servicios/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NoLoginGuard implements CanActivate {

  constructor(
    private router : Router,
    private AFauth : AngularFireAuth,
    private authservice: AuthService
    ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

     
      if(localStorage.getItem('idusuario')!=null && localStorage.getItem('idusuario')!="")
      {
        this.router.navigate(['/home_result']);
        this.authservice.parametros = {
          'nombre': localStorage.getItem('nomuser'),
          'imagen': localStorage.getItem('imguser')
        };
        this.authservice.createUser(this.authservice.parametros);
        return false;
      }
      else{
        return true;
      }
   
      /*return this.AFauth.authState.pipe(map(auth => {

        if(isNullOrUndefined(auth)){
         
         return true;
        }else{
         this.router.navigate(['/home_result']);
          return false;
        }
 
       }))*/

  }
}
