import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../servicios/auth.service';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-modal-solucionar',
  templateUrl: './modal-solucionar.component.html',
  styleUrls: ['./modal-solucionar.component.scss'],
})
export class ModalSolucionarComponent implements OnInit {
  formSolucionar: FormGroup;
  resultado;
  validation_messages = {
    'Solucion': [
      { type: 'required', message: 'Debes ingresar la solución' }
    ]
  };

  Solucion:string;

  constructor(
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,

  ) { }
  ngOnInit() {
    this.resetSolucionar();
  }
  guardarActualizar()
  {
    let Solucion = this.authservice.nl2br(this.Solucion);
    let task = {
      Id: localStorage.getItem('IdTask'),
      Detalle: "",      
      Asignar: "",
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   Solucion,
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'ACTUALIZARSOLUCION',
      color: '#FFFFFF'
    };

    this.authservice.executeTask(task)/*task,Detalle,value.Asignar,localStorage.getItem('idusuario'))*/
    .subscribe((newTask) => {
      this.resultado = JSON.parse(JSON.stringify(newTask));
      if(this.resultado.res)
      {
        this.authservice.error2(this.resultado.msn);
      }
      else
      {
        this.authservice.ok2(this.resultado.msn);
        this.activeModal.close("Cerrar modal y actualizar contador");
        let idtarea = this.resultado.idtarea;
        let para =  this.resultado.para;
        let asi =  this.resultado.asignada;
        const datos = {
          asignada:asi,
          para: para,
          mensaje: "Se actualizo tarea N°"+ idtarea,
          estado: "0",
          tipo: "2",
          idtarea:idtarea
        }
        if(localStorage.getItem('idusuario')!=asi)
        {
          this.authservice.create_recordatorioTask(datos)
          .then(
            () => {
              this.authservice.ok2("Se registro recordatorio al actualización de tarea");
            }).catch(err => {
                this.authservice.error2(err);
            });
        }
      }
      //this.listatareas$.unshift(newTask);
    });

    /*this.authservice.create_abastecimiento(datos)
    .then(
      res => {
        this.authservice.ok 2('Abastecimiento guardado correctamente');
        this.reset_registro();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });*/
    this.formSolucionar = this.formBuilder.group({
      Solucion: new FormControl('', Validators.required),
    });
  }
  guardarSolucionar(value) {
    //
    let Solucion = this.authservice.nl2br(value.Solucion);
    let task = {
      Id: localStorage.getItem('IdTask'),
      Detalle: "",      
      Asignar: "",
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   Solucion,
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'EJECUTARTAREA2',
      color: '#FFFFFF'
    };

    this.authservice.executeTask(task)/*task,Detalle,value.Asignar,localStorage.getItem('idusuario'))*/
    .subscribe((newTask) => {
      this.resultado = JSON.parse(JSON.stringify(newTask));
      if(this.resultado.res)
      {
        this.authservice.error2(this.resultado.msn);
      }
      else
      {
        this.authservice.ok2(this.resultado.msn);
        this.activeModal.dismiss("Cerrar modal y actualizar pendientes");
        let idtarea = this.resultado.idtarea;
        let para =  this.resultado.para;
        let asi =  this.resultado.asignada;
        const datos = {
          asignada:asi,
          para: para,
          mensaje: "Se cerro tarea N°" + idtarea,
          estado: "0",
          tipo: "3",
          idtarea:idtarea
        }
        if(localStorage.getItem('idusuario')!=asi)
        {
          this.authservice.create_recordatorioTask(datos)
          .then(
            () => {
              this.authservice.ok2("Se registro recordatorio al cierre de tarea");
            }).catch(err => {
                this.authservice.error2(err);
            });
        }


      }
      //this.listatareas$.unshift(newTask);
    });

    /*this.authservice.create_abastecimiento(datos)
    .then(
      res => {
        this.authservice.ok2('Abastecimiento guardado correctamente');
        this.reset_registro();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });*/
    this.formSolucionar = this.formBuilder.group({
      Solucion: new FormControl('', Validators.required),
    });
    
  }
  resetSolucionar()
  {
    this.formSolucionar = this.formBuilder.group({
      Solucion: new FormControl('', Validators.required)
    });
  }
}
