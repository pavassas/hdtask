export interface Task {
    Id?:         string;
    Detalle:    any;
    Asignar:    any;
    Estado:     number;
    Creacion:   string;
    Cierre:     string;
    Asignada:   string,
    Para:       string,
    Numreco:    number;
    Env:        string;
    Acciones:   boolean;
    Numsol:     number;
    Solucion:   string;
    Eli:        boolean;
    Fer:        boolean;    
    idusuario: string;
    opcion:     string;
    color: string;
}
