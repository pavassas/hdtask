import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../servicios/auth.service';

// tslint:disable-next-line: class-name
export interface vehiculo {
  name: string;
  id: string;
  klcambioaceite: string;
  cilindraje: string;
  gasolina: string;
  imagen: string;
  klvehiculo: string;
  marca: string;
  matricula: string;
  modelo: string;
  nombre: string;
  polizaseguro: string;
  placa: string;
}

@Injectable({
  providedIn: 'root'
})
export class EditVehiculoService {
  
  constructor(
    private db: AngularFirestore,
    private authservice: AuthService,
  ) { }
  read_vehiculo(placa: string) {
    return this.db.collection('vehiculo', ref => ref.where('placa', '==', placa) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculo;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  update_Student(
    id: string,
    klcambioaceite: string,
    cilindraje: string,
    gasolina: string,
    klvehiculo: string,
    marca: string,
    matricula: string,
    modelo: string,
    nombre: string,
    polizaseguro: string) {
      return new Promise ((resolve, reject) => {
        this.db.collection('vehiculo').doc(id).update({
          gasolina: gasolina,
          klcambioaceite: klcambioaceite,
          klvehiculo: klvehiculo,
          marca: marca,
          matricula: matricula,
          modelo: modelo,
          nombre: nombre,
          polizaseguro: polizaseguro,
          cilindraje: cilindraje
        }).then(() => {
          this.authservice.ok2('Modificado');
        }) .catch(err => {
          this.authservice.error2(err);
        });
      });
  }
  read_Gazolinera(usu) {
    return this.db.collection('combustible', ref => ref.where('usuario', '==', usu)).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculo;
        data.id = a.payload.doc.id;
        console.log(data);
        return data;
      });
    }));
  }
}
