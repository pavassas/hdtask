import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase';
import { AuthService } from '../servicios/auth.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface inventario {
  name: string;
  id: string;
}


@Injectable({
  providedIn: 'root'
})
export class SalidaInventarioService {
  myDate: String = new Date().toISOString();
  public disponible: number;
  constructor(
    private db: AngularFirestore,
    private authservice: AuthService,
    private router: Router,
    
   
  ) { }

  guardarSalida(idinven: string, numsalida: number, disponible: number, fech: any, not: string) {
    if ( disponible >= numsalida) {
      this.db.collection('salidasInventario').add({
        idinventario : idinven,
        cantidad: numsalida,
        fecha: fech,
        nota: not,
        estado: 0
      }).then(() => {
        this.db.collection('inventario').doc(idinven).update({
          salidas: firestore.FieldValue.increment(numsalida),
          disponible: firestore.FieldValue.increment(-numsalida)
        });
        this.authservice.ok2('Guardado');
        this.disponible =  this.disponible - numsalida;
      }) .catch(err => {
        this.authservice.error2(err);
      });
    } else {
      this.authservice.error2('La cantidad solicitada supera la cantidad disponible. ' + ' <br>' + ' Disponible: ' + disponible );
    }
  }
  read_Salida(idinven: string) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('salidasInventario', ref => ref.where('idinventario', '==', idinven).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as inventario;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  delete_detalleinventario( idinv: string, id: string, cantidad: number) {
    return this.db.collection('salidasInventario').doc(id).update({
      estado: 1,
      motivo: ''
    }).then(() => {
      this.db.collection('inventario').doc(idinv).update({
        salidas : firestore.FieldValue.increment(-cantidad),
        disponible: firestore.FieldValue.increment(cantidad)
      });
      this.authservice.ok2('Eliminado');
    }) .catch(err => {
      this.authservice.error2(err);
    });

  }
}
