import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase';
import { AuthService } from '../servicios/auth.service';
import { ok } from 'assert';
// tslint:disable-next-line: class-name
export interface inventario {
  name: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class DetalleinventarioService {
  public entradas: number;
  public salidas: number;


  constructor(
    private db: AngularFirestore,
    private authservice: AuthService
  ) { }
  validation_messages = {
    'Cantidad': [
      { type: 'required', message: 'Requerido' }
    ],
    'fecha':[
      {type: '', message: 'Requerido'}
    ],
    'nota':[
      {type: '', message: 'Requerido'}
    ],
  };
  LisdetaInven(idinven: string) {
    return this.db.collection('entradaInventario', ref => ref.where('idinv', '==', idinven) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as inventario;
        data.id = a.payload.doc.id;
        console.log(data);
        return data;
      });
    }));
  }


  guardarDetalleinve(values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('entradaInventario').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }
  delete_detalleinventario(id, cantidad: number, idinve: string) {
    const cantiresul = this.entradas - cantidad;

    if (cantiresul < this.salidas) {
      this.authservice.error2('No es posible eliminar esta cantidad debido a que la salida no puede ser mayo a lacantidad de entrada');
    } else {
      return this.db.collection('entradaInventario').doc(id).delete().then(() => {
        this.db.collection('inventario').doc(idinve).update({
          entradas : firestore.FieldValue.increment(-cantidad),
          disponible: firestore.FieldValue.increment(-cantidad)
        });
        this.authservice.ok2('Eliminado');
      }) .catch(err => {
        this.authservice.error2(err);
      });
    }
  }
}
