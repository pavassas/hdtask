import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { promise } from 'protractor';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ActionSheetController, Events, ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { map, flatMap } from 'rxjs/operators';
import {BehaviorSubject, Observable, combineLatest } from 'rxjs';
import * as moment from 'moment';

import { DatePipe, formatDate } from '@angular/common';
import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;
import { HttpClient } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Task } from "../interfaces/task";


export interface vehiculo {
  nombre: string;
  id: string;
  idVehiculo: string;
  kmfinal: string
}
export interface usuario{
  id: string;
  nombre: string;
  apellido: string;
  idVehiculo: string;
  imagen: string;
  kmfinal: string;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private api = 'https://pavas.com.co/TAREAS/api.php';
  public idvehisel: any;

  public idususel:any;
  public idusuario: string;
  public imguser: string;
  

  private _loading$ = new BehaviorSubject<boolean>(true);



  public tipos: any =  []; // Array<TiposSeleccionados>;//tipos segun la categoria seleccionados
  public totaltipo: Observable<Number>;
  public parametros: any = [];
  private snapshotChangesSubscription: any;
  registroCollection : AngularFirestoreCollection<usuario>;
  registroItem: Observable<usuario[]>;
  public counta: any;
  public fechaformateada: any;
  // private totaltipo = new BehaviorSubject<number>(0);
  constructor(
    private AFauth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    public events: Events,
    public toastController: ToastController,
    private http: HttpClient,
    @Inject(LOCALE_ID) private locale: string
    ) {}

  getUser(email: string, password: string)
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=VALIDARUSUARIO&email='+email+'&pass='+password);
  }
  getForwotPassword(email:string)
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=RECUPERARCONTRASENA&email='+email);
  }

  getContador(idusuario)
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=CONTADORTAREAS&idusuario='+idusuario);
  }

  getTareas(est:string,idusuario:string,usu,start,lim,bus:string)
  {
    return this.http.get<Task[]>('https://pavas.com.co/TAREAS/api.php?opcion=LISTARTAREAS&idusuario='+idusuario+'&est='+est+'&tec='+usu+'&start='+start+'&lim='+lim+"&bus="+bus);
  }
  getUsuarios()
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=CARGARASIGNAR');
  }

  getDetalle(opc:string,id:string,idusuario:string)
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=' + opc + '&id=' + id+'&idusuario='+idusuario);
  }

  createTask(/*task: Task,*/Detalle:string,Asignar:string,idusuario:string)
  {
    const path = `${this.api}/GUARDAR`;/*<Task[]>*/
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=GUARDAR&Asignar='+Asignar+'&Detalle='+Detalle+'&idusuario='+idusuario);
    //return this.http.post<Task>(path, task);
  }

  /*updateTask(task: Task) {
    const path = `${this.api}/${task.Id}`;
    //return this.http.put<Task>(path, task);

    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion='+task.opcion+'&Asignar='+Asignar+'&Detalle='+Detalle+'&idusuario='+idusuario);
  }*/

  executeTask(task: Task)//guardar,ejecutartarea,ejecutartarea2,APROBARTAREA,ACTUALIZARSOLUCION,ELIMINARTAREA,RECHAZARTAREA,ENVIARRECORDATORIO
  {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion='+task.opcion+'&id='+task.Id+'&sol='+task.Solucion+'&Asignar='+task.Asignar+'&Detalle='+task.Detalle+'&mot='+task.Solucion+'&idusuario='+task.idusuario);
  }

  

  deleteTask(id: string,idusuario:string) {
    return this.http.get('https://pavas.com.co/TAREAS/api.php?opcion=ELIMINARTAREA&id='+id + '&idusuario='+idusuario);
  }

  nl2br(text: string) {
    //return text.replace(/\\r\\n|\\r|\\n/gi, '<br/>');
    //return text.replace('\\r\\n', '<br/>')}
    return text.split("\n").join("<br/>");
   //or ...'<br/>');
  }
  create_recordatorioTask(values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('recordatorioTask').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
  login(email: string, password: string) {
 

    return new Promise((resolve, rejected) => {
      this.getUser(email,password).subscribe(
        (data)=>{
          console.log(data);
        },
        (error) =>{
          console.error(error);
        }
      )
      
     /* this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
        this.idusuario = user.user.uid;
        this.vehisel(this.idusuario).subscribe( usu => {
          this.idvehisel = usu[0].idVehiculo;
          this.imguser = usu[0].imagen;
          localStorage.setItem('idUsuario', user.user.uid);
          localStorage.setItem('idVehiculo', this.idvehisel);
          localStorage.setItem('imguser', this.imguser);
          localStorage.setItem('nomuser', usu[0].nombre + ' ' + usu[0].apellido );
          localStorage.setItem('kmActual', usu[0].kmfinal);
          console.log('Id Vehiculo:' + this.idvehisel);
          console.log('Imagen user:' + this.imguser);
          //console.log(localStorage.getItem('nomuser'));
          this.parametros={
            'nombre': usu[0].nombre + ' ' + usu[0].apellido,
            'imagen': this.imguser
          }
          this.createUser(this.parametros);


        });
        
        //
      }).catch(err => rejected(err));*/

    });
  }

  logout() {
    localStorage.removeItem('idusuario');
    this.router.navigate(['/login']);
    /*this.AFauth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });*/
  }

  register(email: string, password: string, name: string , apellido: string, imagen: string) {
 // fechanaci : string,celular : string,direccion : string
    return new Promise ((resolve, reject) => {
      this.AFauth.auth.createUserWithEmailAndPassword(email, password).then( res => {
          // console.log(res.user.uid);
          const uid = res.user.uid;
          localStorage.setItem('idUsuario', res.user.uid);
          localStorage.setItem('idVehiculo','');
          this.parametros = {
            'nombre': name + ' ' + apellido,
            'imagen': imagen
          };
          this.createUser(this.parametros);
          this.db.collection('users').doc(uid).set({
            nombre: name,
            apellido: apellido,
            // fechanaci :fechanaci,
            // celular:celular,
            // direccion:direccion,
            uid: uid,
            imagen: imagen,
            idVehiculo: ''
          });
          resolve(res);
      }).catch( err => reject(err));
    });
  }
  vehisel(idusu: string) {
    /*return this.db.collection('users', ref1 => ref1.where('uid', '==', idusu) ).snapshotChanges().pipe(map(rooms1 => {
      return rooms1.map(a1 => {
        const dt = a1.payload.doc.data() as usuario;
        dt.id = a1.payload.doc.id;
        return dt;
      });
    }));*/

    this.registroCollection = this.db.collection('users',ref=>ref.where("uid","==",idusu));
    this.registroItem = this.registroCollection.snapshotChanges().pipe(map(changes  => {
      return changes.map( change => {
        const data = change.payload.doc.data();
        const idv = data.idVehiculo;
        const nombre = data.nombre;
        const apellido = data.apellido;
        const imagen = data.imagen;
        const id = change.payload.doc.id;

        // const title = data.title;
          return this.db.doc('vehiculo/' + idv).valueChanges().pipe(map( (vhData: vehiculo) => {
            return Object.assign(
              {idVehiculo:idv,nombre: nombre, apellido, imagen,kmfinal:vhData.kmfinal}); }
          ));
      });
    }), flatMap(feeds => combineLatest(feeds)));
  
    return this.registroItem;      
  }

  async error2(msn: string) {
    const toast = await this.toastController.create({
      header: 'Error',
      message: msn,
      duration: 3000,
      cssClass: 'bg-danger',
      position: 'top'
    });
    toast.present();
  }
  async ok2(msn: string) {
    const toast = await this.toastController.create({
      message: msn,
      duration: 2000
    });
    toast.present();
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function() {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      const imageRef = storageRef.child('image').child(randomId);
      // tslint:disable-next-line: only-arrow-functions
      this.encodeImageUri(imageURI, function(image64) {
        imageRef.putString(image64, 'data_url')
        .then(snapshot => {
          snapshot.ref.getDownloadURL()
          .then(res => resolve(res));
        }, err => {
          reject(err);
        });
      });
    });
  }

  replaceImage(imageURI, randomId,imageUriAct){
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      firebase.storage().refFromURL(imageUriAct).delete();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function(image64) {
        imageRef.putString(image64, 'data_url')
        .then(snapshot => {
          snapshot.ref.getDownloadURL()
          .then(res => resolve(res));
        }, err => {
          reject(err);
        });
      });
    });
  }
  // metodos añadir tipos de servicios
  addTipo(value, ind) {
    if ( ind >= 0 ) {
      this.tipos.splice(ind, 1);
    }
    if ( value.ck == true) {
      console.log(value.ck);
      this.tipos.push(value);
    } else {
      // no se agrega debido a que esta en false
    }
    if ( this.tipos.length > 0) {
      this.totaltipo  = this.tipos.reduce((actual, anterior) =>  parseFloat(actual) + parseFloat(anterior.valor), 0);
      this.createTotal(this.totaltipo);
      console.log(this.totaltipo);
      // this.totaltipo.next();
    }
  }

  edit_perfil(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.AFauth.user.subscribe(currentUser => {
        if ( currentUser ) {
          this.snapshotChangesSubscription = this.db.doc<any>('users/' + id).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
        }
      });
    });
  }

  update_perfil(id, values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('users').doc(id).set(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  createUser(user) {
    this.events.publish('user:login', user, Date.now());
  }

  createTotal(total) {
    this.events.publish('registro:total', total, Date.now());
  }
  count_recordatorio(opt) {

    var fecha = new Date();
    var fecha2 = new Date();
    fecha.setMonth(fecha.getMonth()+11);
    //addTime = tiempo * 86400; //Tiempo en segundos


    console.log(fecha + 'fehca mas un mes');
    console.log(fecha2 + 'fehca Actual');

    if(opt=="tareanueva")
    {
      console.log('si entro recordatorio tarea nueva');
     
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorioTask', ref => ref.where('para', '==', localStorage.getItem('idusuario')).where('estado', '==', "0").where('tipo', '==', "1")).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }
    if(opt=="tareaactualizada")
    {
      console.log('si entro recordatorio tarea nueva');
     
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorioTask', ref => ref.where('asignada', '==', localStorage.getItem('idusuario')).where('estado', '==', "0").where('tipo', '==', "2")).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }
    if(opt=="tareacompletada")
    {
      console.log('si entro recordatorio tarea nueva');
     
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorioTask', ref => ref.where('asignada', '==', localStorage.getItem('idusuario')).where('estado', '==', "0").where('tipo', '==', "3")).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }

    if(opt=="tarearecordatorio")
    {
      console.log('si entro recordatorio tarea nueva');
     
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorioTask', ref => ref.where('para', '==', localStorage.getItem('idusuario')).where('estado', '==', "0").where('tipo', '==', "4")).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }


    if (opt == 'km') {
      console.log('si entro');
      const klactual = localStorage.getItem('kmActual');
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0).where('kmetro', '<=',  parseInt(klactual)).where('fecha1', '==', false).where('kmetro', '>', 0)).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }

    if (opt == 'fecha') {
      const start = new Date();
      console.log(start);
      // tslint:disable-next-line: max-line-length
      return this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0).where('fecha', '<=', start)).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data();
          return data;
        });
      }));
    }
  }

 setFecha(date) {
    console.log('date', moment(date).format('YYYY-MM-DD')); // 2019-04-22
 }
 transform(timestamp: Timestamp, format?: string){
  return ( formatDate(timestamp.toDate(), format || 'medium', this.locale));
}

}
