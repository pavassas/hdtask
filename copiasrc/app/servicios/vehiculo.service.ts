import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { AuthGuard } from './../guards/auth.guard';
import { AuthService } from '../servicios/auth.service';
// tslint:disable-next-line: class-name
export interface vehiculos {
  name: string;
  id: string;
  kmcambioaceite: string;
  cilindraje: string;
  // gasolina: string;
  imagen: string;
  kmvehiculo: string;
  marca: string;
  matricula: string;
  modelo: string;
  nombre: string;
  polizaseguro: string;
  placa: string;
  usuario: string;
}
@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  constructor(
    private AFauth: AngularFireAuth,
    private db: AngularFirestore,
    private authservice: AuthService,
  ) { }

  create_Vehiculo(values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('vehiculo').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
  /*create_Vehiculo(
    nomvehi: string,
    marca: string,
    cilindraje: string,
    modelo: string,
    matricula: string,
    polizaseguro: string ,
    klcambioaceite: string,
    klvehiculo: string,
    gasolina: string,
    idu: string,
    placa: string,
    imagen: string
    ) {
    this.db.collection('vehiculo').add({
      nombre: nomvehi,
      marca: marca,
      cilindraje: cilindraje,
      modelo: modelo, 
      matricula: matricula, 
      polizaseguro: polizaseguro ,
      klcambioaceite: klcambioaceite,
      klvehiculo: klvehiculo,
      gasolina: gasolina,
      usuario: idu,
      placa: placa,
      estado: 0,
      imagen: imagen
      //idVehiculo: this.authservice.idvehisel
    }).then(() => {
      this.authservice.ok2('Eliminado');
    }) .catch(err => {
      this.authservice.error2(err);
    });
  }*/
  read_Vehiculos(usu) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('vehiculo', ref => ref.where('usuario', '==', usu).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculos;
        data.id = a.payload.doc.id;
        console.log(data);
        return Object.assign(
          {
            id: data.id,
            nombre: data.nombre,
            placa: data.placa,
            modelo: data.modelo,
            kmcambioaceite: data.kmcambioaceite,
            cilindraje: data.cilindraje,
            kmvehiculo: data.kmvehiculo,
            marca: data.marca,
            matricula: data.matricula,
            polizaseguro: data.polizaseguro,
            imagen: data.imagen,
            open: false
          });
      });
    }));
  }

  update_VhiculoSel(usu,vehiculo) {
    return new Promise ((resolve, reject) => {
      this.db.collection('users').doc(usu).update({
        idVehiculo : vehiculo
      });
      this.authservice.idvehisel = vehiculo;
      localStorage.setItem('idVehiculo',vehiculo);
    });
  }

  delete_Vehiculo(usu, vehiculo) {
    return new Promise ((resolve, reject) => {
      this.db.collection('vehiculo').doc(vehiculo).update({
        estado : '1'
      });
    });
  }

  
  getVehiculos(usu) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('vehiculo', ref => ref.where('usuario', '==', usu).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculos;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  grVhiculoSel(usu,vehiculo){
    console.log(usu);
    return new Promise ((resolve, reject) => {
      this.db.collection('users').doc(usu).update({
        idVehiculo : vehiculo
      });
      this.authservice.idvehisel = vehiculo;
      localStorage.setItem('idVehiculo',vehiculo);
    });
  }


  private snapshotChangesSubscription: any;

  read_vehiculo(placa: string) {
    return this.db.collection('vehiculo', ref => ref.where('placa', '==', placa) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculos;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }

  read_Gazolinera(usu) {
    return this.db.collection('combustible', ref => ref.where('usuario', '==', usu)).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as vehiculos;
        data.id = a.payload.doc.id;
        console.log(data);
        return data;
      });
    }));
  }
  edit_vehiculo(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.AFauth.user.subscribe(currentUser => {
        if ( currentUser ) {
          this.snapshotChangesSubscription = this.db.doc<any>('vehiculo/' + id).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
        }
      });
    });
  }
  update_vehiculo(id, values) {
    console.log(values);
    return new Promise<any>((resolve, reject) => {
      this.db.collection('vehiculo').doc(id).update(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
