import { Injectable } from '@angular/core';
import { AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { map, flatMap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { DatePipe } from '@angular/common';
import { Recordatorio, Tipo } from '../models/alerta';
import { Observable, combineLatest} from 'rxjs';

// tslint:disable-next-line: class-name
export interface recordatorio {
  nombre: string;
  id: string;
  titulo: string;
  selcate: string;
  seltipo: string;
  repetir: string;
  fecha: string;
  mes: string;
  km: string;
  fecha1: string;
  tiempo: string;
  kmetro: string;
  // tslint:disable-next-line: ban-types
  idnotificacion: Number;
  idvehiculo: string;
  estado: string
}

@Injectable({
  providedIn: 'root'
})
export class RecordatorioService {
  private snapshotChangesSubscription: any;
  public counta: any;
  registroCollection: AngularFirestoreCollection<Recordatorio>;
  registroItem: Observable<Recordatorio[]>;
  constructor(
    private db: AngularFirestore,
    private authservice: AuthService,
    private AFauth: AngularFireAuth,
    private datePipe: DatePipe,
    
  ) { }

  count_recordatorio() {
    this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')))
    .snapshotChanges()
    .subscribe(c => {
      this.counta = c.length + 1;
      // console.log(c.length +1);
      console.log(this.counta);
      return c.length + 1;
    });
  }
  // tslint:disable-next-line: max-line-length
  /*create_recordatorio( Titulo: string, Selcate: string, Seltipo: string, Repetir: number, Fecha: string, Mes: string, Km: number, Fecha1: string, Tiempo: number, Kmetro: number) {

    // tslint:disable-next-line: triple-equals

    this.db.collection('recordatorio').add({
      titulo: Titulo,
      selcate: Selcate,
      seltipo: Seltipo,
      repetir: Repetir,
      fecha: Fecha,
      mes: Mes,
      km: Km,
      fecha1: Fecha1,
      tiempo: Tiempo,
      estado: 0,
      kmetro: Kmetro,
      idvehiculo: localStorage.getItem('idVehiculo'),
      notificacion: 0,
    }).then(() => {
      this.authservice.ok2('Guardado exitosamente');
    }) .catch(err => {
      this.authservice.error2(err);
    });
  }*/

  listar_recordatorio(tip) {
    // tslint:disable-next-line: triple-equals
    if ( tip == 'ejecutados') {
    // tslint:disable-next-line: max-line-length
    this.registroCollection = this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 2));
    } else {
      // tslint:disable-next-line: max-line-length
      this.registroCollection = this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0));
    }

    this.registroItem = this.registroCollection.snapshotChanges().pipe(map(changes  => {
      return changes.map( change => {
        const data = change.payload.doc.data();
        const estado = data.estado;
        const fecha = data.fecha;
        const fecha1 = data.fecha1;
        const idnotificacion = data.idnotificacion;
        const km = data.km;
        const kmetro = data.kmetro;
        const mes = data.mes;
        const notificacion = data.notificacion;
        const repetir = data.repetir;
        const catId = data.selcate;
        const tip = data.seltipo;
        const tiempo = data.tiempo;
        const titulo = data.titulo;
        const open = false;
        const id = change.payload.doc.id;
        const idvehiculo = data.idvehiculo;
        // tslint:disable-next-line: triple-equals

        // const title = data.title;
        return this.db.doc('categorias/' + catId + '/tipos/' + tip).valueChanges().pipe(map( (tipoData: Tipo) => {
            return Object.assign(
              {
                estado: estado,
                fecha: fecha,
                fecha1: fecha1,
                idnotificacion: idnotificacion,
                km: km,
                kmetro: kmetro,
                mes: mes,
                notificacion: notificacion,
                repetir: repetir,
                tiempo: tiempo,
                titulo: titulo,
                id: id,
                tipo: tipoData.nombre,
                idtipo: tip,
                idcategoria: catId,
                idvehi: idvehiculo,
                open: false
              }); }
          ));
      });
    }), flatMap(feeds => combineLatest(feeds)));
    return this.registroItem;
  }

  delete_recordatorio(id: string) {
    return new Promise ((resolve, reject) => {
      this.db.collection('recordatorio').doc(id).update({
        estado: 1
      }).then(() => {
        this.authservice.ok2('Eliminado correctamente');
      }) .catch(err => {
        this.authservice.error2(err);
      });
    });
  }

  read_recordatorio(id: string) {
    return this.db.doc('recordatorio/' + id).valueChanges().pipe(map((com: recordatorio) => {
      return com;
    }));
  }

  edit_recordatorio(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.AFauth.user.subscribe(currentUser => {
        if ( currentUser ) {
          this.snapshotChangesSubscription = this.db.doc<any>('recordatorio/' + id).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
        }
      });
    });
  }

  update_recordatorio(id, values) {
    console.log(values);
    return new Promise<any>((resolve, reject) => {
      this.db.collection('recordatorio').doc(id).set(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
  create_recordatorio(values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('recordatorio').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
