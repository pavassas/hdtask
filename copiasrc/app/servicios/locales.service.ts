import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase';
import { AuthService } from '../servicios/auth.service';
// tslint:disable-next-line: class-name
export interface locales {
  name: string
  id: string
}

@Injectable({
  providedIn: 'root'
})
export class LocalesService {

  constructor(private AFauth: AngularFireAuth, private db: AngularFirestore, private authservice: AuthService) { }

  getLocales(idu: string) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('lugares', ref => ref.where('usuario', '==', idu).where('tipo', '==', 1).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as locales;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  GuardarLocales(nombre: string, ubicacion: string, idu: string) {
    let num = 0;
    // tslint:disable-next-line: max-line-length
    return this.db.collection('lugares', ref => ref.where('nombre', '==', nombre).where('usuario', '==', idu) ).snapshotChanges().pipe(map(rooms => {
      if (rooms.length > 0 ) {
        num += num;
        return num;
      } else {
        this.db.collection('lugares').add({
          nombre: nombre,
          ubicacion: ubicacion,
          usuario: idu,
          tipo: 1,
          estado: 0
        });
        return num += 2;
      }
    }));
  }

  delete_local(id: string) {
    return this.db.collection('lugares').doc(id).update({
      estado : 2
    }).then(() => {
      this.authservice.ok2('Eliminado');
    }) .catch(err => {
      this.authservice.error2(err);
    });
  }

  create_local(values) {
      // tslint:disable-next-line: max-line-length
      return this.db.collection('lugares', ref => ref.where('nombre', '==', values.nombre).where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
        console.log( 'si entro');
        if (rooms.length > 0 ) {console.log( values.nombre); } else {
          console.log( values.nombre);
          this.db.collection('lugares').add(values).then(() => {
            this.authservice.ok2('Guardado');
          }) .catch(err => {
            this.authservice.error2(err);
          });
        }
    }));
  }
}
