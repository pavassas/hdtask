import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map, flatMap } from 'rxjs/operators';
import { Observable, combineLatest} from 'rxjs';
import { Recordatorio, Tipo } from '../models/alerta';

@Injectable({
  providedIn: 'root'
})
export class NotificationsComponentServiceService {
  registroCollection: AngularFirestoreCollection<Recordatorio>;
  registroItem: Observable<Recordatorio[]>;
  registroItem2: Observable<Recordatorio[]>;

  constructor(
    private db: AngularFirestore,
    ) { }

  read_Alerta() {

    // tslint:disable-next-line: max-line-length
    /* this.registroCollection = this.db.collection('recordatorio', ref => ref.where('idvehiculo', '==', localStorage.getItem('idVehiculo')).where('estado', '==', 0)); */

    const klactual = localStorage.getItem('kmActual');
    // tslint:disable-next-line: max-line-length
    this.registroCollection = this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0).where('kmetro', '<=',  parseInt(klactual)).where('fecha1', '==', false).where('kmetro', '>', 0)); 

    this.registroItem = this.registroCollection.snapshotChanges().pipe(map(changes  => {
      return changes.map( change => {
        const data = change.payload.doc.data();
        const estado = data.estado;
        const fecha = data.fecha;
        const fecha1 = data.fecha1;
        const idnotificacion = data.idnotificacion;
        const km = data.km;
        const kmetro = data.kmetro;
        const mes = data.mes;
        const notificacion = data.notificacion;
        const repetir = data.repetir;
        const catId = data.selcate;
        const tip = data.seltipo;
        const tiempo = data.tiempo;
        const titulo = data.titulo;
        const open = false;
        const id = change.payload.doc.id;
        const idvehiculo = data.idvehiculo;
        // const title = data.title;
        return this.db.doc('categorias/' + catId + '/tipos/' + tip).valueChanges().pipe(map( (tipoData: Tipo) => {
          console.log(tipoData);
            // tslint:disable-next-line: align
            return Object.assign(
              {
                Estado: estado,
                Fecha: fecha,
                Fecha1: fecha1,
                Idnotificacion: idnotificacion,
                Km: km,
                Kmetro: kmetro,
                Mes: mes,
                Notificacion: notificacion,
                Repetir: repetir,
                Tiempo: tiempo,
                Titulo: titulo,
                Open: open,
                Id: id,
                Tipo: tipoData.nombre,
                idtipo: tip,
                idcategoria: catId,
                idvehi: idvehiculo
              }); }
          ));
      });
    }), flatMap(feeds => combineLatest(feeds)));

    const start = new Date();
    
    // tslint:disable-next-line: max-line-length
    this.registroCollection = this.db.collection('recordatorio', ref => ref.where('usuario', '==', localStorage.getItem('idUsuario')).where('estado', '==', 0).where('fecha', '<=', start));

    this.registroItem = this.registroCollection.snapshotChanges().pipe(map(changes  => {
      return changes.map( change => {
        console.log('entro por acá segunda consulta');
        const data = change.payload.doc.data();
        const estado = data.estado;
        const fecha = data.fecha;
        const fecha1 = data.fecha1;
        const idnotificacion = data.idnotificacion;
        const km = data.km;
        const kmetro = data.kmetro;
        const mes = data.mes;
        const notificacion = data.notificacion;
        const repetir = data.repetir;
        const catId = data.selcate;
        const tip = data.seltipo;
        const tiempo = data.tiempo;
        const titulo = data.titulo;
        const open = false;
        const id = change.payload.doc.id;
        const idvehiculo = data.idvehiculo;
        // const title = data.title;
        console.log(id);
        return this.db.doc('categorias/' + catId + '/tipos/' + tip).valueChanges().pipe(map( (tipoData: Tipo) => {
          console.log(tipoData);
            // tslint:disable-next-line: align
            return Object.assign(
              {
                Estado: estado,
                Fecha: fecha,
                Fecha1: fecha1,
                Idnotificacion: idnotificacion,
                Km: km,
                Kmetro: kmetro,
                Mes: mes,
                Notificacion: notificacion,
                Repetir: repetir,
                Tiempo: tiempo,
                Titulo: titulo,
                Open: open,
                Id: id,
                Tipo: tipoData.nombre,
                idtipo: tip,
                idcategoria: catId,
                idvehi: idvehiculo
              }); }
          ));
      });
    }), flatMap(feeds => combineLatest(feeds)));
    return this.registroItem;
}
/*read_Alertaejecutar(id) {
  this.registroItem2 = this.db.collection('recordatorio').doc(id).snapshotChanges().pipe(map(changes  => {
    return changes.map( change => {
      const data = change.payload.doc.data();
      const catId = data.selcate;
      const fecha = data.fecha;
      const tip = data.seltipo;
      const estado = data.estado;
      const titulo = data.titulo;
      const open = false;
      const id = change.payload.doc.id;
      // const title = data.title;
      return this.db.doc('categorias/'+catId+'/tipos/'+tip).valueChanges().pipe(map( (tipoData: Tipo) => {
        console.log(tipoData);
          // tslint:disable-next-line: align
          return Object.assign(
            {id:id,titulo:titulo,idtipo:tip, tipo: tipoData.nombre, fecha: fecha, estado: estado }); }
        ));
    });
  }), flatMap(feeds => combineLatest(feeds)));

  return this.registroItem2;
}*/


}
