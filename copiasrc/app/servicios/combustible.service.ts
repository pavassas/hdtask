import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { AuthService } from '../servicios/auth.service';

// tslint:disable-next-line: class-name
export interface combustible {
  nombre: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class CombustibleService {
  private snapshotChangesSubscription: any
  constructor(
    private AFauth: AngularFireAuth,
    private db: AngularFirestore,
    private authservice: AuthService ) { }
    GuardarCombustible(nomcombus: string, idu: string) {
      let num = 0;
      // tslint:disable-next-line: max-line-length
      return this.db.collection('combustible', ref => ref.where('nombre', '==', nomcombus).where('usuario', '==', idu) ).snapshotChanges().pipe(map(rooms => {
        if (rooms.length > 0 ) {
          num += num;
          return num;
        } else {
           this.db.collection('combustible').add({
              nombre: nomcombus,
              usuario: idu,
              estado: 0
            });
           return num += 2;
        }
      }));
    }
    getCombustible(usu) {
      // tslint:disable-next-line: max-line-length
      return this.db.collection('combustible', ref => ref.where('usuario', '==', usu).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as combustible;
          data.id = a.payload.doc.id;
          return data;
        });
      }));
    }
    delete_combustible(id: string) {
      return this.db.collection('combustible').doc(id).update({
        estado : 1
      }).then(() => {
        this.authservice.ok2('Eliminado');
      }) .catch(err => {
        this.authservice.error2(err);
      });
    }
    edit_Combustible(id){
      return new Promise<any>((resolve, reject) => {          
        this.snapshotChangesSubscription = this.db.doc<any>('combustible/' +id).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err)
        })
      });
    }
    /*read_locales(id: string){
      return this.db.doc('combustible/' + id).valueChanges().pipe(map((com: combustible) => {
        return com;
      }));
    }*/
    update_combustible(id,value) {
      return new Promise<any>((resolve, reject) => {        
        this.db.collection('combustible').doc(id).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        )
      })
    }
    create_combustible(values) {
      return new Promise<any>((resolve, reject) => {
        this.db.collection('combustible').add(values)
        .then(
          res => resolve(res),
          err => reject(err)
        );
      });
    }
}


