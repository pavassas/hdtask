import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { flatMap,map } from 'rxjs/operators';
import { AuthService } from '../servicios/auth.service';// tslint:disable-next-line: class-name
import { Registro,Categoria } from '../models/registro';
import { Observable,combineLatest} from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/storage';

import { Router } from '@angular/router';
import { LocalNotifications, ILocalNotificationActionType } from '@ionic-native/local-notifications/ngx';


export interface tipos {
  name: string;
  id: string;
}
export interface categoria {
  nombre: string;
  id: string;
}

export interface local {
  nombre: string;
  id: string;
}

/*export interface registro {
  categoria: string;
  fecha:string;
  hora:string;
  local:string;
  nota:string;
  id: string;
  detalle:Array<String>
}*/

@Injectable({
  providedIn: 'root'
})

export class RegistroService {
  private snapshotChangesSubscription: any
  registroCollection : AngularFirestoreCollection<Registro>;
  registroItem: Observable<Registro[]>;
  registroItem2: Observable<Registro[]>;

  constructor(
    private AFauth: AngularFireAuth,
    private db: AngularFirestore,
    private authservice: AuthService,
    private router: Router,
    private localNotifications: LocalNotifications,
    ) {
      this.read_Registro();

     }
    create_Registro(datos) {// nombre: string, ubicacion: string, idu: string
        this.db.collection('registro').add(datos).then(() => {
          // tslint:disable-next-line: radix
          if (parseInt(localStorage.getItem('kmActual')) < datos.km ) {
            this.db.collection('vehiculo').doc(localStorage.getItem('idVehiculo')).update({
              kmfinal: datos.km
            });
            localStorage.setItem('kmActual', datos.km);
          }
          return 1;
        }).catch(
          err => {
            return 2;
          }
        );
        return 1;
    }

    ejecutar_Registro(datos,rep,mes,idn) {
      //console.log(datos.idrecordatorio);

      this.db.collection('registro').add(datos).then((res) => {
       // console.log(res.id)
       if (rep == 2 ) {
        const fechanueva = new Date(datos.fecha.setMonth(datos.fecha.getMonth() + parseInt(mes)));
        this.localNotifications.update({
          id: idn,
          title: 'Recordatorio Pavas MotoR',
          foreground: true,
          trigger: {at: new Date(fechanueva.getTime() + 10000)},
        });
        this.db.collection('recordatorio').doc(datos.idrecordatorio).update({ fecha: fechanueva });
       }
       else {
        this.db.collection('recordatorio').doc(datos.idrecordatorio).update({ estado: 2 });
       }


        this.router.navigate(['/actualizar-registro/' + res.id + '/2']);
        return 1;
      }).catch(

      );
      return 1;
  }
    read_Tipos(cat, search: string) {
      // tslint:disable-next-line: max-line-length
      /*return this.db.collection('categorias').doc(cat).collection('tipos',ref=>ref.where("nombre","==",search)).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as tipos;
          data.id = a.payload.doc.id;
          return data;
        });
      }));*/
      return this.db.collection('categorias').doc(cat).collection('tipos', ref => ref
      .orderBy('nombre')
      .startAt(search.toLowerCase())
      .endAt(search.toLowerCase() + '\uf8ff')
      .limit(10)).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as tipos;
          data.id = a.payload.doc.id;
          return data;
        });
      }));
     /* return this.db.collection('categorias').doc(cat).collection('tipos', ref => ref
      .orderBy("nombre")
      .startAt(search.toLowerCase())
      .endAt(search.toLowerCase()+"\uf8ff")
      .limit(10))
      .valueChanges();*/
    }

    read_Registro() {
        if ( this.authservice.idvehisel == '' || this.authservice.idvehisel == undefined) {
          this.authservice.idvehisel = localStorage.getItem('idVehiculo');
        }
        this.registroCollection = this.db.collection('registro',ref=>ref.where('idvehiculo', '==', localStorage.getItem('idVehiculo')));
        this.registroItem = this.registroCollection.snapshotChanges().pipe(map(changes  => {
          return changes.map( change => {
            const data = change.payload.doc.data();
            const catId = data.categoria;
            const fecha = data.fecha;
            const nota = data.nota;
            const local = data.local;
            const open = false;
            const id = change.payload.doc.id;
            // const title = data.title;
            return this.db.doc('categorias/' + catId).valueChanges().pipe(map( (catData: Categoria) => {
                return Object.assign(
                  // tslint:disable-next-line: max-line-length
                  { id: id, categoria: catData.nombre, catId: catId, fecha: fecha, nota: nota ,detalle: data.detalle, local: local, open: open } ); }
              ));
          });
        }), flatMap(feeds => combineLatest(feeds)));
        return this.registroItem;
    }
    getNomCategoria(cat) {
      return this.db.collection('categorias').doc(cat).snapshotChanges().pipe(map(cate => {
        const data = cate.payload.data() as categoria;
        data.id = cate.payload.id;
        // console.log(data);
        return data;
      }));
    }
    getNomLocal(loc) {
      return this.db.collection('lugares').doc(loc).snapshotChanges().pipe(map(loca => {
        const data = loca.payload.data() as local;
        data.id = loca.payload.id;
        // console.log(data);
        return data;
      }));
    }

    delete_Registro(idr: string) {
      this.db.collection('registro').doc(idr).delete()
      .then(() => {
        this.authservice.ok2('Registro eliminado correctamente');
        this.read_Registro();
      })
      .catch(err => {
        this.authservice.error2('Error:' + err);
      });
    }

    edit_Registro(id) {
      return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.db.doc<any>('registro/' + id).valueChanges()
            .subscribe(snapshots => {
              resolve(snapshots);
            }, err => {
              reject(err);
            });
      });
    }

    update_Registro(id, value) {
      return new Promise<any>((resolve, reject) => {
        this.db.collection('registro').doc(id).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
      }).then(() => {
        // tslint:disable-next-line: radix
        if ( parseInt(localStorage.getItem('kmActual')) < value.km ) {
          console.log('123');
          this.db.collection('vehiculo').doc(localStorage.getItem('idVehiculo')).update({
            kmfinal: value.km
          });
        }
      }
      );
    }
}


