import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { AuthService } from '../servicios/auth.service';



// tslint:disable-next-line: class-name
export interface gasolinera {
  id: string;
  nombre: string;
  ubicacion: string;
}

@Injectable({
  providedIn: 'root'
})
export class GasolineraService {
  private snapshotChangesSubscription: any;
  constructor(
    private AFauth: AngularFireAuth,
    private db: AngularFirestore,
    private authservice: AuthService,
  ) { }
  GuardarGasolinera(nombre: string, ubicacion: string, idu: string) {
    let num = 0;
    // tslint:disable-next-line: max-line-length
    return this.db.collection('lugares', ref => ref.where('nombre', '==', nombre).where('usuario', '==', idu).where('tipo', '==', 2)).snapshotChanges().pipe(map(rooms => {
      if (rooms.length > 0 ) {
        num += num;
        return num;
      } else {
        console.log('entro por acá');
        this.db.collection('lugares').add({
          nombre: nombre,
          ubicacion: ubicacion,
          usuario: idu,
          tipo: 2,
          estado: 0
        });
        return num += 2;
      }
    }));
  }
  getGasolinera(idu: string) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('lugares', ref => ref.where('usuario', '==', idu).where('tipo', '==', 2).where('estado', '==', 0) ).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as gasolinera;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  delete_gasolinera(id: string) {
    return this.db.collection('lugares').doc(id).update({
      estado : 2
    }).then(() => {
      this.authservice.ok2('Eliminado');
    }) .catch(err => {
      this.authservice.error2(err);
    });
  }
  edit_gasolinera(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.AFauth.user.subscribe(currentUser => {
        if ( currentUser ) {
          this.snapshotChangesSubscription = this.db.doc<any>('lugares/' + id).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
        }
      });
    });
  }
  update_gasolinera(id, values) {
    console.log(values);
    return new Promise<any>((resolve, reject) => {
      this.db.collection('lugares').doc(id).set(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
  create_gasolinera(values) {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('lugares').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
