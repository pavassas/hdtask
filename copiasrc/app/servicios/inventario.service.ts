import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { AuthService } from '../servicios/auth.service';
import { Router } from '@angular/router';
// tslint:disable-next-line: class-name
export interface inventario {
  name: string;
  id: string;
  idVehiculo: string;
}

@Injectable({
  providedIn: 'root'
})

export class InventarioService {
  private snapshotChangesSubscription: any;
  constructor(
    private AFauth: AngularFireAuth,
    private db: AngularFirestore,
    private authservice: AuthService,
    private router: Router
    ) { }
    GuardarInventario( nombre: string, marca: string, almacen: string, fecha: any, valor: string, cantidad: string, idu: string) {
      let num = 0;
      // tslint:disable-next-line: max-line-length
      return this.db.collection('inventario', ref => ref.where('nombre', '==', nombre).where('idvehi', '==', localStorage.getItem('idVehiculo')).where('estado', '==', 0 ) ).snapshotChanges().pipe(map(rooms => {
        if (rooms.length > 0 ) {
          num += num;
          return num;
        } else {
         // tslint:disable-next-line: no-unused-expression
         new Promise<any>((resolve, reject) => {
            this.db.collection('inventario').add({
              nombre: nombre,
              idvehi: localStorage.getItem('idVehiculo'),
              estado: 0,
              entradas: cantidad,
              salidas: 0,
              disponible: cantidad
            }).then(
              res => {
                this.db.collection('entradaInventario').add({
                  marca: marca,
                  almacen: almacen,
                  fecha: fecha,
                  cantidad: cantidad,
                  valor: valor,
                  idinv: res.id
                }).then(() => {
                  this.authservice.ok2('Articulo guardado correctamente');
                  this.router.navigate(['/inventario']);
                }) .catch(err => {
                  this.authservice.error2(err);
                });
              },
            );
          });
        }
      }));
    }
    LisInvent(idvehiculo) {
      // tslint:disable-next-line: max-line-length
      return this.db.collection('inventario', ref => ref.where('idvehi', '==', idvehiculo).where('estado', '==', 0)).snapshotChanges().pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as inventario;
          data.id = a.payload.doc.id;
          return data;
        });
      }));
    }
    edit_inventario(id: string) {
      return new Promise<any>((resolve, reject) => {
        this.AFauth.user.subscribe(currentUser => {
          if ( currentUser ) {
            this.snapshotChangesSubscription = this.db.doc<any>('inventario/' + id).valueChanges()
            .subscribe(snapshots => {
              resolve(snapshots);
            }, err => {
              reject(err);
            });
          }
        });
      });
    }
    update_inventario(id, values) {
      console.log(values);
      return new Promise<any>((resolve, reject) => {
        this.db.collection('inventario').doc(id).update(values)
        .then(
          res => resolve(res),
          err => reject(err)
        );
      });
    }

    Eliminar_inventario(id) {
      return this.db.collection('inventario').doc(id).delete().then(() => {
        this.authservice.ok2('Eliminado');
      }) .catch(err => {
        this.authservice.error2(err);
      });
     /* return new Promise<any>((resolve, reject) => {
        this.db.collection('inventario').doc(id).update({
          estado: 1
        })
        .then(
          res => resolve(res),
          err => reject(err)
        );
      });*/
    }

}


