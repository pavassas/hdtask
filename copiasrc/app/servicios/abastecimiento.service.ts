import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

export interface abaste {
  name: string;
  id: string;

  fecha: string,
  kilometro: string,
  hora: string,
  gasolina: string,
  precio: number,
  galon: number,
  Total: number,
  gasolinera: string,
  nota: string
}
export interface vehiculo {
  name: string;
  id: string;
  gasolina: string;

}

@Injectable({
  providedIn: 'root'
})
export class AbastecimientoService {
  private snapshotChangesSubscription: any;
  constructor(
    private db: AngularFirestore,
    public authservice: AuthService,
    private AFauth: AngularFireAuth
  ) { }


  read_Gazolinera(usu) {
    return this.db.collection('combustible', ref => ref.where('usuario', '==', usu)).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as abaste;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }

  read_loclagazolina(usu) {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('lugares', ref => ref.where('usuario', '==', usu).where('tipo', '==', 2)).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as abaste;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }

  // tslint:disable-next-line: max-line-length
  read_Abastecimiento() {
    // tslint:disable-next-line: max-line-length
    return this.db.collection('abastecimiento', ref => ref.where('idvehiculo', '==', localStorage.getItem('idVehiculo')).where('estado', '==', 0)).snapshotChanges().pipe(map(rooms => {
      return rooms.map(a => {
        const data = a.payload.doc.data() as abaste;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }
  delete_Abastecimiento(idabastecimiento) {
    return new Promise ((resolve, reject) => {
      this.db.collection('abastecimiento').doc(idabastecimiento).update({
        estado : 1
      });
    });
  }
  read_tipogas(idvehiculo) {
    // consulta de un solo registo
    console.log(idvehiculo);
    return this.db.collection('vehiculo').doc(idvehiculo).snapshotChanges().pipe(map(rooms => {
      const data = rooms.payload.data() as vehiculo;
      data.id = rooms.payload.id;
      return data;
    }));
  }/*
  read_abaste(id: string) {
    return this.db.doc('abastecimiento/' + id).valueChanges().pipe(map((com: abaste) => {
      return com;
    }));
  }*/
  // tslint:disable-next-line: max-line-length
  update_abastecimiento(id, values ) {
    console.log(values);
    return new Promise<any>((resolve, reject) => {
      this.db.collection('abastecimiento').doc(id).update(values)
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
  edit_abastecimienti(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.AFauth.user.subscribe(currentUser => {
        if ( currentUser ) {
          this.snapshotChangesSubscription = this.db.doc<any>('abastecimiento/' + id).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
        }
      });
    });
  }
  create_abastecimiento(values){
    return new Promise<any>((resolve, reject) => {
      this.db.collection('abastecimiento').add(values)
      .then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }
}
