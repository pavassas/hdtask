import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { map } from "rxjs/operators";
import { firestore } from 'firebase';
export interface categoria {
  name : string
  id: string
}
export interface tipos {
  name : string
  id: string
}

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private db : AngularFirestore) { }

  getCategorias(){
    return this.db.collection('categorias').snapshotChanges().pipe(map(rooms => {
      return rooms.map(a =>{
        const data = a.payload.doc.data() as categoria;
        data.id = a.payload.doc.id;
        return data;
      })
    }))
  }


  getTipos(cat){
    return this.db.collection('categorias').doc(cat).collection('tipos').snapshotChanges().pipe(map(rooms => {
      return rooms.map(a =>{
        const data = a.payload.doc.data() as tipos;
        data.id = a.payload.doc.id;
        return data;
      })
    }))
  }
}
