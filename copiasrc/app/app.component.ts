import { Component } from '@angular/core';

import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Pages } from './interfaces/pages';
import { AuthService } from './servicios/auth.service';

import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;
  public nombreu: string;
  public apellido: string;
  public foto: string;
  public imagen: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private authservices: AuthService,
    public events: Events,
    private config: NgSelectConfig
    
    ) {
    this.config.placeholder = 'Seleccionar item';
    // This could be useful if you want to use appendTo in entire application without explicitly defining it. (eg: appendTo = 'body')
    this.config.appendTo = null; 
    this.config.notFoundText = 'No encuentra datos';
    this.config.loadingText = 'Cargando datos';
    
    this.appPages = [
      {
        title: 'Inicio',
        url: '/home-results',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Mi cuenta',
        url: '/cuenta',
        direct: 'forward',
        icon: 'car'
      }
    
    ];

    this.initializeApp();
    events.subscribe('user:login', (user, time) => {
      console.log(user);
      this.nombreu =user.nombre;
      this.imagen = user.imagen;
     
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }).catch(() => {});
    //this.nombre = localStorage.getItem('nomuser');
    //console.log(this.nombre)
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
    this.authservices.logout();
  }
}
