import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { RecordatorioService } from 'src/app/servicios/recordatorio.service';


@Injectable()
export class RecordatorioResolver implements Resolve<any> {

  constructor(
    private mRecor: RecordatorioService,
      ) {}

      resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
          let itemId = route.paramMap.get('id');
          this.mRecor.edit_recordatorio(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          })
        })
      }
}
