import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RecordatorioResolver } from './actualizar-recordatorio.resolver';


import { IonicModule } from '@ionic/angular';

import { ActualizarRecordatorioPage } from './actualizar-recordatorio.page';

const routes: Routes = [
  {
    path: '',
    component: ActualizarRecordatorioPage,
    resolve: {
      data: RecordatorioResolver
    }
  }
];

@NgModule({
  imports: [
  CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarRecordatorioPage],
  providers:[RecordatorioResolver]
})
export class ActualizarRecordatorioPageModule {}
