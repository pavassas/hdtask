import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RecordatorioService } from 'src/app/servicios/recordatorio.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { ToastController } from '@ionic/angular';
import { CategoriaService } from './../../servicios/categoria.service';

import { LocalNotifications, ILocalNotificationActionType } from '@ionic-native/local-notifications/ngx';

//import undefined = require('firebase/empty-import');




@Component({
  selector: 'app-actualizar-recordatorio',
  templateUrl: './actualizar-recordatorio.page.html',
  styleUrls: ['./actualizar-recordatorio.page.scss'],
})
export class ActualizarRecordatorioPage implements OnInit {
  formRecordatorio: FormGroup;
  public idrecor: string;
  public liscombus: any = [];
  public repi1: boolean  = false;
  public repi2: boolean  = true;
  public mece: any = [];
  item: any;
  public selrepi: string;
  public tipoList: any = [];
  public cateList: any = [];
  public selcate: string;
  public km: boolean;
    // valores de input
  public inputDisabledmk: boolean = true;
  public inputDisabledfec: boolean = true;
  public inputDisabledmes: boolean = true;

  public chkm: boolean = false;
  public chfecha1: boolean = false;
  public chtiempo: boolean = false;


  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public mRecor: RecordatorioService,
    private activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    private authService: AuthService,
    public toastController: ToastController,
    public cate: CategoriaService,
    private localNotifications: LocalNotifications,


  ) { }
  validation_messages = {
    'selcate': [
      { type: 'required', message: 'requerido' }
    ],
    'seltipo': [
      { type: 'required', message: 'requerido' }
    ],
    'repetir': [
      { type: 'required', message: 'requerido' }
    ],
    'titulo': [
      {type: 'required', message: 'requerido'}
    ],

  };

  ngOnInit() {
    this.getData();
    this.MESES();
    /*this.idrecor = this.activatedRoute.snapshot.paramMap.get('id');
    this.mRecor.read_recordatorio(this.idrecor).subscribe( data => {
      this.liscombus = data;
    });*/
    this.cate.getCategorias().subscribe( cat => {
      this.cateList = cat;
    });
  }
  getData() {
    this.route.data.subscribe(routeData => {
     const data = routeData['data'];
     if (data) {
       this.item = data;
       this.selcate = this.item.selcate;
       this.cargartipo();
       if (this.item.repetir == 1) {
        this.repi1 = true;
        this.repi2 = false;
        } else {
        this.repi1 = false;
        this.repi2 = true;
        }
       this.km = this.item.km;
     }
    });

    const fech = this.item.fecha;
    
    const fech2 = (fech != '')? this.authService.transform(fech) : '';

    console.log(fech2);

    this.formRecordatorio = this.formBuilder.group({
      titulo: new FormControl(this.item.titulo),
      selcate: new FormControl(this.item.selcate),
      seltipo: new FormControl(this.item.seltipo),
      repetir: new FormControl(this.item.repetir),
      fecha: new FormControl(fech2),
      mes: new FormControl(this.item.mes),
      km: new FormControl(this.item.km),
      fecha1: new FormControl(this.item.fecha1),
      tiempo: new FormControl(this.item.tiempo),
      kmetro: new FormControl(this.item.kmetro),
    });

    this.inputDisabledfec = this.item.fecha1 ? false : true;
    this.inputDisabledmk = this.item.km ? false : true;
    this.inputDisabledmes = this.item.tiempo ? false : true;

    this.chfecha1 = this.item.fecha1 ? true : false;
    this.chkm = this.item.km ? true : false;
    this.chtiempo = this.item.tiempo ? true : false;
  }

  GUARDAREDITRECORDATORIO(value) {
    let fechaseleccionada: any;

    // tslint:disable-next-line: triple-equals
    // tslint:disable-next-line: max-line-length
    if ((this.chkm == false) && ( this.chfecha1 == false )&&( this.chtiempo == false )){
        this.authService.error2('Debes de seleccionar una opción métrica de recordatorio');
    } else {

      console.log('si entro');

      if (this.chkm == false) {  value.kmetro = 0; } else { }


      // tslint:disable-next-line: max-line-length
      if (this.chfecha1 == false && parseInt( value.repetir) == 1)  { fechaseleccionada = ''; } else if (parseInt( value.repetir) == 1 ) {
        fechaseleccionada = new Date(value.fecha);
        this.chtiempo = false;
        value.mes = '';
      }

      if (this.chtiempo == false &&  parseInt( value.repetir) == 2) { } else if (parseInt( value.repetir) == 2) {
        this.chfecha1 = false;
        const fecha = new Date();
        const fechaseleccionada2 = fecha.setMonth(fecha.getMonth() + parseInt(value.mes));
        fechaseleccionada = new Date(fechaseleccionada2);
      }

      console.log(fechaseleccionada);
      const datos = {
        titulo: value.titulo,
        selcate: value.selcate,
        seltipo: value.seltipo,
        repetir: value.repetir,
        fecha: fechaseleccionada,
        mes: value.mes,
        km: this.chkm,
        fecha1: this.chfecha1,
        tiempo: this.chtiempo,
        // tslint:disable-next-line: radix
        kmetro: parseInt(value.kmetro),
        estado: this.item.estado,
        idvehiculo: localStorage.getItem('idVehiculo'),
        notificacion: this.item.notificacion,
        usuario: this.item.usuario,
        idnotificacion: this.item.idnotificacion
      };
      this.mRecor.update_recordatorio(this.item.id, datos).then(
        res => {

          // tslint:disable-next-line: triple-equals
          if ( fechaseleccionada != '' && fechaseleccionada != undefined) {
                const ano = new Date(value.fecha).getFullYear();
                const mes = new Date(value.fecha).getMonth();
                const dia = new Date(value.fecha).getDay();
                this.localNotifications.update({
                  id: this.item.idnotificacion,
                  title: 'Recordatorio Pavas MotoR',
                  text: value.titulo,
                  foreground: true,
                  trigger: {at: new Date(new Date(fechaseleccionada).getTime() + 3600)},
                });
              }

          this.toastController.create({
            message: 'Actualizado correctamente',
            duration: 2000
          }).then((toastData) => {
            toastData.present().then(() => {
              this.router.navigate(['/recordatorio']);
            });
          });
        }
      ).catch(er => {
        this.authService.error2(er);
      });
    }
  }

  segmentChanged(ev: any) {
    this.selrepi = this.formRecordatorio.get('repetir').value;
    if (this.formRecordatorio.get('repetir').value == 1) {
    this.repi1 = true;
    this.repi2 = false;
    } else {
    this.repi1 = false;
    this.repi2 = true;
    }
  }

  MESES() {
    for (let v = 1; v <= 60; v++ ) {
      this.mece.push(v);
    }
  }

  cargartipo() {
    this.cate.getTipos(this.selcate).subscribe( tip => {
      this.tipoList = tip;
    });
  }

  closeInventario() {
    this.router.navigate(['/recordatorio']);
  }

  VALIDARTIPO(values) {
    this.inputDisabledfec = values.fecha1 ? false : true;
    this.inputDisabledmk = values.km ? false : true;
    this.inputDisabledmes = values.tiempo ? false : true;

    this.chfecha1 = values.fecha1 ? true : false;
    this.chkm = values.km ? true : false;
    this.chtiempo = values.tiempo ? true : false;
  }

}
