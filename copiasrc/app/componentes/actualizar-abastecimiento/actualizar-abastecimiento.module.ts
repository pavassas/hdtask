import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActualizarAbastecimientoPage } from './actualizar-abastecimiento.page';
import { AbastecimientoResolver } from './actualizar-abastecimiento.resolver';

const routes: Routes = [
  {
    path: '',
    component: ActualizarAbastecimientoPage,
    resolve: {
      data: AbastecimientoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarAbastecimientoPage],
  providers: [AbastecimientoResolver]
})
export class ActualizarAbastecimientoPageModule {}
