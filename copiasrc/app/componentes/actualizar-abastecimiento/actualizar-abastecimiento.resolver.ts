import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AbastecimientoService } from './../../servicios/abastecimiento.service';


@Injectable()
export class AbastecimientoResolver implements Resolve<any> {

  constructor(
    private meAbastesimietno: AbastecimientoService,

      ) {}

      resolve(route: ActivatedRouteSnapshot) {
        return new Promise((resolve, reject) => {
          let itemId = route.paramMap.get('id');
          this.meAbastesimietno.edit_abastecimienti(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          });
        });
      }
}