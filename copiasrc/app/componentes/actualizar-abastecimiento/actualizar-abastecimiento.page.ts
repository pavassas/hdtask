import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AbastecimientoService } from 'src/app/servicios/abastecimiento.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-actualizar-abastecimiento',
  templateUrl: './actualizar-abastecimiento.page.html',
  styleUrls: ['./actualizar-abastecimiento.page.scss'],
})
export class ActualizarAbastecimientoPage implements OnInit {
  formAbas: FormGroup;
  public idabast: string;
  public lisabas: any;
  public lislocalgaso: any = [];
  public idusuario: string;
  public lisgasoli: any = [];
  item: any;
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private mAbaste: AbastecimientoService,
    private activatedRoute: ActivatedRoute,
    private AFauth: AngularFireAuth,
    private route: ActivatedRoute,
    public toastController: ToastController,
    private authService: AuthService,
  ) { }
  validation_messages = {
    'Fecha': [
      { type: 'required', message: 'Requerido' }
    ],
    'Kilometro': [
      { type: 'required', message: 'Requerido' }
    ],
    'Hora': [
      { type: 'required', message: 'Requerido' }
    ],
    'Gasolina': [
      { type: 'required', message: 'Requerido' }
    ],

    'Precio': [
      { type: 'required', message: 'Requerido' }
    ],

    'Galon':[
      { type: 'required', message: 'Requerido' }
    ],
    'Gasolinera': [
      { type: 'required', message: 'Requerido' }
    ],
    'Nota': [
      { type: 'required', message: 'Requerido' }
    ],
  };
  ngOnInit() {
    this.idabast = this.activatedRoute.snapshot.paramMap.get('id');
    this.LISTARLOCALGASOLINA();
    this.LISTARGAZOLINERAS();
    this.getData();
  }
  LISTARLOCALGASOLINA() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.mAbaste.read_loclagazolina(this.idusuario).subscribe( lgas => {
          this.lislocalgaso = lgas;
        });
      });
    });
  }
  LISTARGAZOLINERAS() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.mAbaste.read_Gazolinera(this.idusuario).subscribe( gas => {
          this.lisgasoli = gas;
        });
      });
    });
  }
  GUARDAREDITABASTE(values) {
    const fec = new Date(values.Fecha);
    const datos = {
      fecha: values.fec,
      kilometro: values.Kilometro,
      hora: values.Hora,
      gasolina: values.Gasolina,
      precio: values.Precio,
      galon: values.Galon,
      gasolinera: values.Gasolinera,
      nota: values.Nota,
      idvehiculo: this.item.idvehiculo,
      estado: this.item.estado,
      Total: values.Total
    };
    this.mAbaste.update_abastecimiento(this.item.id, datos).then(
      res => {
        this.toastController.create({
          message: 'Actualizado correctamente',
          duration: 2000
        }).then((toastData) => {
          toastData.present().then(() => {
            this.router.navigate(['/abastecimiento']);
          });
        });
      }
    ).catch(er => {
      this.authService.error2(er);
    });

  }

  getData() {
    this.route.data.subscribe(routeData => {
     let data = routeData['data'];
     if (data) {
       this.item = data;
     }
    });
    this.formAbas = this.formBuilder.group({
      Fecha: new FormControl(this.item.fecha, Validators.required),
      Kilometro: new FormControl(this.item.kilometro, Validators.required),
      Hora: new FormControl(this.item.hora, Validators.required),
      Gasolina: new FormControl(this.item.gasolina, Validators.required),
      Precio: new FormControl(this.item.precio, Validators.required),
      Galon: new FormControl(this.item.galon, Validators.required),
      Total: new FormControl(this.item.Total),
      Gasolinera: new FormControl(this.item.gasolinera, Validators.required),
      Nota: new FormControl(this.item.nota, Validators.required),
    });
  }

}
