import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { ToastController, ActionSheetController, LoadingController } from '@ionic/angular';
import { AuthService } from './../../servicios/auth.service';
import { VehiculoService } from './../../servicios/vehiculo.service';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-actualizar-vehiculo',
  templateUrl: './actualizar-vehiculo.page.html',
  styleUrls: ['./actualizar-vehiculo.page.scss'],
})
export class ActualizarVehiculoPage implements OnInit {
 
  vehilis: any = [];
  gaslis: any = [] ;
  gasolina2: any[];
  idusuario: string;
  formeditar: FormGroup;
  image: any;
  public placa: string;
  public id: string;
  public nombre: String;
  public marca: String;
  public cilin: String;
  public modelo: String;
  public matricu: String;
  public poliza: String;
  public klcambio: String;
  public klvehi: String;
  public gaso: String;
  public anomax: any  = new Date().getFullYear()+1;
  public anomin: any  = new Date().getFullYear()-20;
  item: any;
  public info1: boolean  = true;
  public info2: boolean  = false;
  constructor(
    private AFauth: AngularFireAuth,
    private editvehiculoservice: VehiculoService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public toastController: ToastController,
    private authService: AuthService,
    private route: ActivatedRoute,
    private webview: WebView,
    private crop: Crop,
    private camera: Camera,
    private file: File,
    public actionSheetController: ActionSheetController,
    public loadingCtrl: LoadingController,

    ) { }
    validation_messages = {
      'cilindraje2': [
        { type: 'required', message: 'La marca es requerido' },
      ],
      'modelo2': [
        { type: 'required', message: 'El cilindraje es requerido' }
      ],
    };
    ngOnInit() {
      this.placa = this.activatedRoute.snapshot.paramMap.get('placa');
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.LISTARGAZOLINERAS2();
      this.getData();
    }
    getData() {
      this.route.data.subscribe(routeData => {
       let data = routeData['data'];
       if (data) {
         this.item = data;
         this.image = this.item.imagen;
       }
      }); 
      this.formeditar = this.formBuilder.group({
        nomvehi2: new FormControl(this.item.nombre, Validators.required),
        marca2: new FormControl(this.item.marca, Validators.required),
        cilindraje2: new FormControl(this.item.cilindraje, Validators.required),
        modelo2: new FormControl(this.item.modelo, Validators.required),
        matricula2: new FormControl(this.item.matricula),
        polizaseguro2: new FormControl(this.item.polizaseguro),
        klcambioaceite2: new FormControl(this.item.kmcambioaceite),
        klvehiculo2: new FormControl(this.item.kmvehiculo),
        placa: new FormControl(this.item.placa),
        tipo: new FormControl(this.item.tipo),
      });
    }
    EDIGUARDAR(value) {
      const datos = {
        nombre: value.nomvehi2,
        marca: value.marca2,
        cilindraje: value.cilindraje2,
        modelo: value.modelo2,
        matricula: value.matricula2,
        polizaseguro: value.polizaseguro2,
        klcambioaceite: value.klcambioaceite2,
        klvehiculo: value.klvehiculo2,
        usuario: localStorage.getItem('idUsuario'),
        placa: value.placa,
        estado: this.item.estado,
        imagen: this.item.imagen,
        kmfinal: this.item.kmfinal,
        tipo: this.item.tipo
      };
      console.log(datos);
      this.editvehiculoservice.update_vehiculo(this.item.id, datos).then(
        res => {
          this.toastController.create({
            message: 'Actualizado correctamente',
            duration: 2000
          }).then((toastData) => {
            toastData.present().then(() => {
              this.router.navigate(['/vehiculo']);
            });
          });
        }
      ).catch(er => {
        this.authService.error2(er);
      });
    }
    LISTARGAZOLINERAS2() {
      return new Promise<any>((resolve, rejected) => {
        this.AFauth.user.subscribe(currentuser => {
            this.idusuario = currentuser.uid;
            this.editvehiculoservice.read_Gazolinera(this.idusuario).subscribe( gas => {
            this.gasolina2 = gas;
            console.log(this.gasolina2);
          });
        });
      });
    }
    masinfo(op1, op2) {
      console.log(op1, op2);
      this.info1 = op1;
      this.info2 = op2;
    }
    pickImage(sourceType) {
      const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        // let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.cropImage(imageData)
        }, (err) => {
        // Handle error
        }
      
        );
      }
    async selectImage() {
      const actionSheet = await this.actionSheetController.create({
        header: "Seleccionar recurso",
        buttons: [{
          text: 'Mostrar galeria',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar Camara',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
  
    cropImage(fileUrl) {
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          this.authService.error2('Error cropping image' + error);
        }
      );
    }
    showCroppedImage(ImagePath) {
      //this.isLoading = true;
      var copyPath = ImagePath;
      var splitPath = copyPath.split('/');
      var imageName = splitPath[splitPath.length - 1];
      var filePath = ImagePath.split(imageName)[0];
  
      this.file.readAsDataURL(filePath, imageName).then(base64 => {
        //this.croppedImagepath = base64;//this.image
        //this.isLoading = false;
        this.uploadImageToFirebase(base64);
  
      }, error => {
        this.authService.error2('Error in showing image' + error);
        //this.isLoading = false;
      });
    }
  
    async uploadImageToFirebase(image){
      const loading = await this.loadingCtrl.create({
        message: 'Espere por favor...'
      });
      const toast = await this.toastController.create({
        message: 'Imagen cargada correctamente',
        duration: 3000
      });
      this.presentLoading(loading);
      let image_src = this.webview.convertFileSrc(image);
      let randomId = Math.random().toString(36).substr(2, 5);
  
      //uploads img to firebase storage
      /*this.auth.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err =>{
        console.log(err);
      })*/
      
      if(this.image!='./assets/img/imgvehiculo.png')
      {    
        this.authService.replaceImage(image_src, randomId,this.image)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
          this.updateVehiculo();
        }, err =>{
         
          console.log(err);
        })
      }
      else
      {
        this.authService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
          this.updateVehiculo()
        }, err =>{
          console.log(err);
         
        }) 
      }
      
    }
    updateVehiculo() {
      const datos = {
        nombre: this.item.nomvehi2,
        marca: this.item.marca2,
        cilindraje: this.item.cilindraje2,
        modelo: this.item.modelo2,
        matricula: this.item.matricula2,
        polizaseguro: this.item.polizaseguro2,
        klcambioaceite: this.item.klcambioaceite2,
        klvehiculo: this.item.klvehiculo2,
        usuario: localStorage.getItem('idUsuario'),
        placa: this.item.placa,
        estado: this.item.estado,
        imagen: this.image,
        kmfinal: this.item.kmfinal
      };
      console.log(datos);
      this.editvehiculoservice.update_vehiculo(this.item.id, datos).then(
        res => {
          console.log('actualizo imagen vehiculo');
        }
      ).catch(er => {
        this.authService.error2(er);
      });
    }
    async presentLoading(loading) {
      return await loading.present();
    }
}
