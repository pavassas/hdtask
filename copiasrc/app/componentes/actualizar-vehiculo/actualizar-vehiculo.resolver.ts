import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { VehiculoService } from './../../servicios/vehiculo.service';

@Injectable()
export class VehiculoResolver implements Resolve<any> {

  constructor(
    private meVehiculo: VehiculoService,
      ) {}

      resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
          let itemId = route.paramMap.get('id');
          this.meVehiculo.edit_vehiculo(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          });
        });
      }
}
