import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActualizarVehiculoPage } from './actualizar-vehiculo.page';
import { VehiculoResolver } from './actualizar-vehiculo.resolver';

const routes: Routes = [
  {
    path: '',
    component: ActualizarVehiculoPage,
    resolve: {
      data: VehiculoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarVehiculoPage],
  providers: [VehiculoResolver]
})
export class ActualizarVehiculoPageModule {}
