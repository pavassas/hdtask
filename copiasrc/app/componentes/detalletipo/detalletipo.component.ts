import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { RegistroService } from '../../servicios/registro.service';
import { AuthService } from '../../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-detalletipo',
  templateUrl: './detalletipo.component.html',
  styleUrls: ['./detalletipo.component.scss'],
})


export class DetalletipoComponent implements OnInit {

  //searchbar = document.querySelector('ion-searchbar');
  searchValue: string = "";
  public cat: string;
  public dettip: any = [];
  //public dettip: Observable<Tipo[]>
  public nomcat:any;
  public nomtipo:Observable<String>;
  public valor = 0;
  constructor(
    private modal: ModalController,
    private navparams: NavParams,
    private regservice:RegistroService ,
    private authservice: AuthService,
    private AFauth: AngularFireAuth
  ) { 
    
  }

  ngOnInit() {
    //this.authservice.tipos.length = 0;
    this.cat = this.navparams.get('cat');
    this.searchTipo();
  }
  searchTipo(){
    this.regservice.read_Tipos(this.cat,this.searchValue).subscribe( deta => {
      this.dettip = deta;   
    });
  }

  seltipo(id:string,nombre:string,ev: any,opc:number)
  {
    console.log(ev);
    let ck = true;
    this.valor = 0;
    if(opc==2)
    {
      this.valor = ev.detail.value;
    }
    else
    {
      ck = ev.detail.checked;
    }  
    
    let ind = -1;
    this.authservice.tipos.forEach(function(element,index){
        console.log(element.nombre + ": " + index);
        if(element.tipo==id)
        {  
          if(opc==2)
          {
            ck = element.ck;
          }        
          else{
            this.valor = element.valor;
          }
          ind = index;
          return;
        }
    });
    let dat = {
      cat:this.cat,
      tipo:id,
      nombre:nombre,
      ck:ck,
      valor:this.valor
    }
    this.authservice.addTipo(dat,ind);
    console.log(this.authservice.tipos);
    
  }

  closeTipo() {
    this.modal.dismiss();
  }
}
