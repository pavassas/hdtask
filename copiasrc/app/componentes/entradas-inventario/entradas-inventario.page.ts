import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DetalleinventarioService } from '../../servicios/detalleinventario.service';
import { AuthService } from '../../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRoute , Router} from '@angular/router';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { LocalesService } from 'src/app/servicios/locales.service';
import { firestore } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-entradas-inventario',
  templateUrl: './entradas-inventario.page.html',
  styleUrls: ['./entradas-inventario.page.scss'],
})
export class EntradasInventarioPage implements OnInit {
  public idinv: string;
  public salida: number;
  public detinvlis: any = [];
  public  marca: string;
  public  fecha: string;
  public  cantidad: number;
  public  valor: string;
  public  candisponible: string;
  formEntrada:FormGroup;
  public canti: number;


  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private meDetInve: DetalleinventarioService,
    private authservice: AuthService,
    private AFauth: AngularFireAuth,
    private activatedRoute: ActivatedRoute,
    public alertController: AlertController,
    public localservice: LocalesService,
    private db: AngularFirestore

  ) { }

  title = 'ng-autocomplete';
  almacen = new FormControl();
  filteredOptions: Observable<string[]>;
  public localList : any = [];

  validation_messages = {
    'marca': [
      { type: 'required', message: 'Requerido' }
    ],
    'almacen':[
      {type: '', message: 'Requerido'}
    ],
    'fecha':[
      {type: '', message: 'Requerido'}
    ],
    'valor':[
      {type: '', message: 'Requerido'}
    ],
    'cantidad':[
      {type: '', message: 'Requerido'}
    ],
  };
  ngOnInit() {
    this.idinv = this.activatedRoute.snapshot.paramMap.get('id');
    // tslint:disable-next-line: radix
    this.meDetInve.salidas = parseInt(this.activatedRoute.snapshot.paramMap.get('salidas'));
    // tslint:disable-next-line: radix
    this.meDetInve.entradas = parseInt(this.activatedRoute.snapshot.paramMap.get('entradas'));
    this.resetEntrada();
    //return new Promise<any>((resolve, rejected) => {
      this.meDetInve.LisdetaInven(this.idinv ).subscribe( deta => {
        this.detinvlis = deta;
      });
    //});

    this.filteredOptions = this.formEntrada.get('almacen').valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    this.localservice.getLocales(localStorage.getItem('idUsuario')).subscribe( loc => {

      this.localList = loc;
      console.log(loc);
    });
    
  }
  private _filter(value: string): string[] {
    const filterValue = value.toString().toLowerCase();
    return this.localList.filter(option => option.nombre.toString().toLowerCase().includes(filterValue));
  }
  resetEntrada()
  {
    this.formEntrada = this.formBuilder.group({
      marca: new FormControl('', Validators.required),
      almacen: new FormControl('', Validators.required),
      fecha: new FormControl(new Date().toISOString(), Validators.required),
      valor: new FormControl('', Validators.required),
      cantidad: new FormControl('', Validators.required),
    });
    console.log(this.formEntrada.get('marca').value);
  }
  GUARDARDETALLEINVENT(values) {
    const fecinven = new Date(values.fecha);
    let datos = {
      marca: values.marca,
      almacen: values.almacen,
      fecha: fecinven,
      cantidad: values.cantidad,
      valor: values.valor,
      idinv: this.idinv
    };
    this.meDetInve.guardarDetalleinve(datos)
    .then(
      res => {
        this.db.collection('inventario').doc(this.idinv).update({
          entradas : firestore.FieldValue.increment(values.cantidad),
          disponible: firestore.FieldValue.increment(values.cantidad)
        });
        const datoslocal = {
          nombre: values.almacen,
          ubicacion: '',
          usuario: localStorage.getItem('idUsuario'),
          tipo: 1,
          estado: 0
        };
        this.localservice.create_local(datoslocal).subscribe( inven => {
          // this.inveLis = inven;
        });
        this.authservice.ok2('Guardado correctamente');
        this.resetEntrada();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }



  closeInventario() {
    this.router.navigate(['/inventario']);
  }
  async ELIMINARDETALLE(id, can) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar la entrada seleccionada!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.meDetInve.delete_detalleinventario(id, can, this.idinv);
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
