import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {GasolineraService } from '../../servicios/gasolinera.service';
import { AuthService } from '../../servicios/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-gasolinera',
  templateUrl: './gasolinera.page.html',
  styleUrls: ['./gasolinera.page.scss'],
})
export class GasolineraPage implements OnInit {
  public Nombre: string;
  public ubicacion: string;
  public idusuario: string;
  public  gasoList: any = [];
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  formGasolinera: FormGroup;

  constructor(
    private AFauth: AngularFireAuth,
    private meGasolinera: GasolineraService,
    private authservice: AuthService,
    public formBuilder: FormBuilder,
    private router: Router,
    public alertController: AlertController,
  ) { }
  validation_messages = {
    'Nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
    'Ubicacion': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
  };

  ngOnInit() {
    this.CARGARCOMBUSTIBLE();
    this.CAMPOS();
  }
  CARGARCOMBUSTIBLE() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meGasolinera.getGasolinera(this.idusuario).subscribe( gasoli => {
          this.gasoList =  gasoli;
        });
      });
    });
  }
  segmentChanged(ev: string) {
    // tslint:disable-next-line: triple-equals
    if (ev == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }

  async ELIMINARGASOLINERA(id: string) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar la gasolinera!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ACEPTAR',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.meGasolinera.delete_gasolinera(id);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  CAMPOS() {
    this.formGasolinera = this.formBuilder.group({
      Nombre: new FormControl('', Validators.required),
      Ubicacion: new FormControl('', Validators.required),
    });
  }

  ACTULIZARGASOLINERA(id: string) {
    this.router.navigate(['/actulizar-gasolinera', id]);
  }
  GUARDARGASOLINERA(value) {
    let datos = {
      nombre: value.Nombre,
      ubicacion: value.Ubicacion,
      usuario: this.idusuario,
      tipo: 2,
      estado: 0
    };
    this.meGasolinera.create_gasolinera(datos)
    .then(
      res => {
        this.authservice.ok2('Gasolinera guardada correctamente');
        this.CAMPOS();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }
}
