import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GasolineraPage } from './gasolinera.page';

describe('GasolineraPage', () => {
  let component: GasolineraPage;
  let fixture: ComponentFixture<GasolineraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GasolineraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GasolineraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
