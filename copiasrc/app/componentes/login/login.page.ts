import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AlertController, ModalController, LoadingController, ToastController, MenuController, NavController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validator';
//import { ImageModalPage } from '../../image-modal/image-modal.page';
import * as firebase from 'firebase/app';
import {firestore} from 'firebase/app';
import { message } from './../../models/message';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;
  onLoginForm: FormGroup;
  login;
  forwot;
  url:string = "https://www.pavas.com.co/HD/";
  public parametros: any = [];
 

  // tslint:disable-next-line: variable-name
  matching_passwords_group: FormGroup;
  public errorMessage = '';
  public successMessage = '';

  sliderOpts = {
    zoom: false,
    slidesPerView: 1.5,
    spaceBetween: 20,
    centeredSlides: true
  };

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email o usuario requerido' },
      { type: 'pattern', message: 'Por favor ingresa un email u usuario valido' }
    ],
    'password': [
      { type: 'required', message: 'Contraseña es requerida' },
      { type: 'minlength', message: 'La contraseña debe tener al menos 5 caracteres de longitud.' },
      { type: 'pattern', message: 'Su contraseña debe contener al menos una mayúscula, una minúscula y un número.' }
    ],
  };

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private authservice: AuthService,
    public router: Router,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private AFauth: AngularFireAuth,
    ) { }
    ionViewWillEnter() {
      this.menuCtrl.enable(false);
    }

  ngOnInit() {
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        // Validators.minLength(5),
        Validators.required,
        // Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      // confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });
    this.onLoginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        //Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group
    });
  }



  onSubmitLogin(value) {

    this.authservice.getUser(value.email,value.matching_passwords.password).subscribe(
      (data)=>{ 
        this.login =   JSON.parse(JSON.stringify(data)); 
        if(this.login.res)
        {
          this.authservice.error2(this.login.msn);
        }
        else
        {
          localStorage.setItem('idusuario', this.login.idusuario);
          localStorage.setItem('imguser', this.url + this.login.imgusuario);
          localStorage.setItem('nomuser', this.login.usuario);
          this.parametros = {
            'nombre': this.login.usuario,
            'imagen': this.url+  this.login.imgusuario
          };
          this.authservice.createUser(this.parametros);
          this.navCtrl.navigateRoot('/home-results');
        }
        console.log(this.login.res);},
      (error)=>{ console.log(error); }
    )

    /*this.authservice.login(value.email, value.matching_passwords.password).then( res =>{
      this.navCtrl.navigateRoot('/home-results');
    }).catch(  err => { this.authservice.error2('Usuario o contraseña incorrectos');console.log('error de logueo');})*/
  }

  openPreview(img) {
    this.modalController.create({
      component: "",//ImageModalPage,
      componentProps: {
        img: img
      }
    }).then(modal => {
      modal.present();
    });
  }

  async forgotPass() {
    // tslint:disable-next-line: prefer-const
    let mensage: string;
    const alert = await this.alertCtrl.create({
      header: 'Olvido contraseña?',
      message: 'Ingresa tu correo eletronico para enviarte un link de restauración de contraseña.',
      inputs: [
        { name: 'email', type: 'email', placeholder: 'Correo electrónico'},
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn btn-danger',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirmar',
          cssClass: 'btn btn-primary',
          handler: async (alertData) => {
            // tslint:disable-next-line: max-line-length
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            const loader = await this.loadingCtrl.create({
              duration: 2000
            });
            loader.present();

            const result = re.test(alertData.email);
            // tslint:disable-next-line: triple-equals
            if (alertData.email == '' || alertData.email == null || alertData.email == undefined){
              mensage = 'Ingrese un correo eletronico';
            } else {
              if (!result) {
                mensage = 'Ingrese un correo eletronico valido';
              } else {
                console.log('si');
                 // tslint:disable-next-line: only-arrow-functions
                 this.authservice.getForwotPassword(alertData.email).subscribe((data)=>{
                  this.forwot =   JSON.parse(JSON.stringify(data)); 
                  mensage = this.forwot.msn;

                 },(error)=>{
                   console.log(error);
                 })
              }
            }
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                message: mensage,
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }
  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }
}
