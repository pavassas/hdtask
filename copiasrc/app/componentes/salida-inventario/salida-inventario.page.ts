import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SalidaInventarioService } from '../../servicios/salida-inventario.service';
import { map } from 'rxjs/operators';
import { AuthService } from '../../servicios/auth.service';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-salida-inventario',
  templateUrl: './salida-inventario.page.html',
  styleUrls: ['./salida-inventario.page.scss'],
})
export class SalidaInventarioPage implements OnInit {
  public idinventario: string;
  formSalida: FormGroup;
  public Cantidad: number;
 public detinvlis: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public formBuilder: FormBuilder,
    private mSali: SalidaInventarioService,
    private authservice: AuthService,
    public alertController: AlertController,
    
    ) { }
    validation_messages = {
      'Cantidad': [
        { type: 'required', message: 'Requerido' }
      ],
      'fecha':[
        {type: '', message: 'Requerido'}
      ],
      'nota':[
        {type: '', message: 'Requerido'}
      ],
    };

  ngOnInit() {
    this.idinventario = this.activatedRoute.snapshot.paramMap.get('id');
    // tslint:disable-next-line: radix
    this.mSali.disponible  = parseInt(this.activatedRoute.snapshot.paramMap.get('disponible'));
    
   
    this.resetSalida();  
    this.mSali.read_Salida(this.idinventario ).subscribe( deta => {
      this.detinvlis = deta;
    });
 
  }
  resetSalida(){
    this.formSalida = this.formBuilder.group({
      fecha: new FormControl(new Date().toISOString(), Validators.required),
      Cantidad: new FormControl('', Validators.required),
      nota: new FormControl('', Validators.required),
    });
  }
  closeInventario() {
    this.router.navigate(['/inventario']);
  }
  GUARDARSALIDA(values) {
    return new Promise<any>((resolve, rejected) => {
        // tslint:disable-next-line: max-line-length
        const fecinven = new Date(values.fecha);
        this.mSali.guardarSalida(
          this.idinventario,
          values.Cantidad,
          this.mSali.disponible,
          fecinven,
          values.nota
        );
    });
  }

  async ELIMINARDETALLE(id, can) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar la entrada seleccionada!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.mSali.delete_detalleinventario(this.idinventario, id, can);
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
