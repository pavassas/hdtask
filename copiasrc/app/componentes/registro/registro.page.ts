import { Component, OnInit } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController, ToastController, ModalController, AlertController, Events } from '@ionic/angular';

import { CategoriaService } from '../../servicios/categoria.service';
import { LocalesService } from '../../servicios/locales.service';
import { AuthService } from '../../servicios/auth.service';
import { RegistroService } from '../../servicios/registro.service';
import { TiposSeleccionados } from './tipos.model';
import { DetalletipoComponent } from '../../componentes/detalletipo/detalletipo.component';
import { Observable } from 'rxjs';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
import { startWith, map } from 'rxjs/operators';



@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  public cateList: any = [];
  public tipoList: any = [];
  public localList: any = [];
  public selcate: string;
  public idusuario: string;
  selcat: string;
  total: any;
  tiposeleccionados: Observable<TiposSeleccionados>[];
  public tipos: any = [];
  // funciones para la validacion
  // tslint:disable-next-line: variable-name
  validations_form: FormGroup;
  // tslint:disable-next-line: variable-name
  matching_passwords_group: FormGroup;
  public errorMessage: string = '';
  mivehiculo: any;
  validation_messages = {
    'selfecha': [
      { type: 'required', message: 'La fecha es requeridad' }
    ],
    'selcate': [
      { type: 'required', message: 'La Categoria es requerida' }
    ],
    'txtnota': [
      { type: 'required', message: 'La nota es requerida' }
    ],
    'sellocal': [
      { type: 'required', message: 'El local es requerido' }
    ],
    'txtlocal': [
      { type: 'required', message: 'El local es requerido' }
    ],
    'txtkm': [
      { type: 'required', message: 'El kilometraje es requerido' }
    ]
  };
// datos listado
public vehiculoList: any = [];
public registroList: any = [];
public registroList2: any = [];
automaticClose = false;

public ocultar1: boolean  = false;
public ocultar2: boolean  = true;

// autocomplet
filteredOptionsLocal: Observable<string[]>;

  constructor(
   public cate: CategoriaService,
   public localservice: LocalesService,
   private AFauth: AngularFireAuth,
   private authservice: AuthService,
   private modal: ModalController,
   private formBuilder: FormBuilder,
   public alertCtrl: AlertController,
    // private imagePicker: ImagePicker,
   public toastCtrl: ToastController,
   public loadingCtrl: LoadingController,
   public registroservice: RegistroService,
   public vehiculoservice: VehiculoService,
   public events: Events

  ) { }

  ngOnInit() {
    this.mivehiculo = localStorage.getItem('idVehiculo');
    this.vehiculoservice.getVehiculos(localStorage.getItem('idUsuario')).subscribe( veh => {
      this.vehiculoList = veh;
    });
    this.cate.getCategorias().subscribe( cat => {
      this.cateList = cat;
    });
    this.cargarlocal();
    this.tiposeleccionados = this.authservice.tipos;
    // this.total = this.authservice.totaltipo;

    console.log('Total:' + this.total);
    this.resetRegistro();
    /*const total = this.authservice.tipos.reduce( (
      acc,
      obj,
    ) => return acc + (obj.valor),
    return 0);*/
    this.events.subscribe('registro:total', (tot, time) => {
      this.total = tot;
    });
  }
  private _filterLocal(value: string): string[] {
    const filterValue = value.toString().toLowerCase();
    return this.localList.filter(option => option.nombre.toString().toLowerCase().includes(filterValue));
  }
  resetRegistro() {
    this.validations_form = this.formBuilder.group({
      selfecha: new FormControl(new Date().toISOString(), Validators.required),
      // sellocal: new FormControl('', Validators.required),
      txtlocal: new FormControl('', Validators.required),
      selcate: new FormControl('', Validators.required),
      txtnota: new FormControl(''),
      txtkm: new FormControl('', Validators.required),
    });
    this.registroservice.read_Registro().subscribe( reg => {
      if (reg.length <= 0) {
        this.registroList.length = 0;
      } else {
        this.registroList = reg;
      }
    });
    this.filteredOptionsLocal = this.validations_form.get('txtlocal').valueChanges
    .pipe(
      startWith(''),
      map(value => this._filterLocal(value))
    );
  }
  opcion(op: string) {
    // tslint:disable-next-line: triple-equals
    if (op == 'nuevo') {
      this.ocultar1 = true;
      this.ocultar2 = false;
    } else {
      this.ocultar1 = false;
      this.ocultar2 = true;
    }
  }
  changeVehiculo() {
    localStorage.setItem('idVehiculo', this.mivehiculo);
    this.authservice.idvehisel = this.mivehiculo;
    this.resetRegistro();
  }
  toggleSection(index) {
    this.registroList[index].open = !this.registroList[index].open;
    if (this.automaticClose && this.registroList[index].open) {
      this.registroList
      // tslint:disable-next-line: triple-equals
      .filter((item, itemIndex) => itemIndex != index)
      .map(item => item.open = false);
    }
  }
  cargartipo() {
      this.cate.getTipos(this.selcate).subscribe( tip => {
        this.tipoList = tip;
        // console.log(tip)
      });
  }

  openTipos() {
    if (this.selcat != this.selcate) {
        this.authservice.tipos.length = 0;
        this.authservice.createTotal(0);
    }
    this.selcat = this.selcate;
    if (this.selcate) {
      const groupModal =  this.modal.create({
        component: DetalletipoComponent,
        componentProps : {
          cat: this.selcate
        }
      }).then( (modal) => modal.present() );
    } else {
      this.authservice.error2('Seleccionar la categoria');
    }
  }
  cargarlocal() {
      return new Promise<any>((resolve, rejected) => {
        this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.localservice.getLocales(this.idusuario).subscribe( loc => {
            this.localList = loc;
            // console.log(loc);
          });
      });
    });
  }
  deleteTip(i) {
    console.log(this.authservice.tipos[i].nombre);
    this.authservice.tipos.splice(i, 1);
    const tot = this.tipos.reduce((actual, anterior) =>  parseFloat(actual) + parseFloat(anterior.valor), 0);
    this.authservice.createTotal(tot);
  }

  OnSubmitRegistro(value) {
     const fec  = new Date(value.selfecha);
     const nui = this.authservice.tipos.length;
     if (localStorage.getItem('idVehiculo') == '' || localStorage.getItem('idVehiculo') == null) {
      this.authservice.error2('No hay un vehiculo seleccionado');
     } else
     if (nui <= 0) {
       this.authservice.error2('No hay detalle seleccionado');
     } else {
      return new Promise<any>((resolve, rejected) => {
        this.AFauth.user.subscribe(currentuser => {
            this.idusuario = currentuser.uid;
            const datos = {
              idvehiculo: localStorage.getItem('idVehiculo'),
              fecha: fec,
              // local:value.sellocal,
              local: value.txtlocal,
              categoria: this.selcate,
              nota: value.txtnota,
              detalle: this.authservice.tipos,
              km:  value.txtkm
            };
            const msn = this.registroservice.create_Registro(datos);
            if (msn == 1) {
              // creacion del local diligenciado
              const datoslocal = {
                nombre: value.txtlocal,
                ubicacion: '',
                usuario: localStorage.getItem('idUsuario'),
                tipo: 1,
                estado: 0
              };
              this.localservice.create_local(datoslocal).subscribe( inven => {
              });
              this.resetRegistro();
              this.authservice.tipos.length = 0;
              this.authservice.ok2('Registro guardado correctamente');
            } else {
              this.authservice.error2('No se guardo el registro');
            }
        });
      });
     }
  }
  async ELIMINARREGISTRO(idregistro: string) {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: 'Deseas eliminar el registro seleccionado!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.AFauth.user.subscribe(currentuser => {
                 this.registroservice.delete_Registro(idregistro);
              });
            });

          }
        }
      ]
    });
    await alert.present();
  }
}
