export interface TiposSeleccionados {
  cat: string;
  tip: string;
  valor: number;
  ck: boolean;
}