import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { CombustibleService } from '../../servicios/combustible.service';

@Injectable()
export class ActualizarCombustibleResolver implements Resolve<any> {

  constructor(public combustibleservice: CombustibleService,) { }

  resolve(route: ActivatedRouteSnapshot) {
    return new Promise((resolve, reject) => {
      let itemId = route.paramMap.get('id');
      this.combustibleservice.edit_Combustible(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      })
    })
  }
}
