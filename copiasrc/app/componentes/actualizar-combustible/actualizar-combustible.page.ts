import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl  } from '@angular/forms';
import { CombustibleService } from 'src/app/servicios/combustible.service';
import { AuthService } from '../../servicios/auth.service';

@Component({
  selector: 'app-actualizar-combustible',
  templateUrl: './actualizar-combustible.page.html',
  styleUrls: ['./actualizar-combustible.page.scss'],
})
export class ActualizarCombustiblePage implements OnInit {
  formcombustible: FormGroup;
  private idcombu: string;
  private liscombus: any;
  item: any;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder ,
    private CombustibleServices: CombustibleService,
    private route: ActivatedRoute,
    private authservice:AuthService
  ) { }

  // tslint:disable-next-line: variable-name
  validation_messages = {
    'Nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
  };

  ngOnInit() {
    /*this.idcombu = this.activatedRoute.snapshot.paramMap.get('id');
    this.formcombustible = this.formBuilder.group({
      Nombre: new FormControl('', Validators.required),
    });
    this.CombustibleServices.read_locales(this.idcombu).subscribe( data => {
      this.liscombus = data;      
    });
    */
    this.resetEdit();
  }
  closeCombustible() {
    this.router.navigate(['/combustible']);
  }
  resetEdit() {
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data;
      }
     })
    this.formcombustible = this.formBuilder.group({
      Nombre: new FormControl(this.item.nombre, Validators.required),
    });
  }
  GUARDAREDITCOM (value) {
    let datos = {             
      nombre: value.Nombre,
      usuario: localStorage.getItem('idUsuario'),
      estado: this.item.estado
    }

    this.CombustibleServices.update_combustible(this.item.id,datos).then(res => {
 
      this.authservice.ok2("Registro modificado correctamente");
      this.router.navigate(["/combustible"]);
    }).catch(err=>{
        this.authservice.error2("No se guardo el registro." + err );
    });
  }
}