import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActualizarCombustiblePage } from './actualizar-combustible.page';
import { ActualizarCombustibleResolver } from './actualizar-combustible.resolve';

const routes: Routes = [
  {
    path: '',
    component: ActualizarCombustiblePage,
    resolve: {
      data: ActualizarCombustibleResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarCombustiblePage],
  providers:[ActualizarCombustibleResolver]
})
export class ActualizarCombustiblePageModule {}
