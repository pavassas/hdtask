import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GasolineraService } from 'src/app/servicios/gasolinera.service';


@Injectable()
export class GasolineraResolver implements Resolve<any> {

  constructor(
    private meGasolinera: GasolineraService,
      ) {}

      resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
          let itemId = route.paramMap.get('id');
          this.meGasolinera.edit_gasolinera(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          })
        })
      }
}
