import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute} from '@angular/router';
import { GasolineraService } from '../../servicios/gasolinera.service';
import { AuthService } from '../../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-actulizar-gasolinera',
  templateUrl: './actulizar-gasolinera.page.html',
  styleUrls: ['./actulizar-gasolinera.page.scss'],
})
export class ActulizarGasolineraPage implements OnInit {

  public idgasolinera: string;
  public lisgaso: any;
  item: any;
  formGasolinera: FormGroup;
  public mece: any = [];

  constructor(

    private activatedRoute: ActivatedRoute,
    private mactulizar: GasolineraService,
    private AFauth: AngularFireAuth,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public toastController: ToastController

  ) { }

  validation_messages = {
    'Nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
    'Ubicacion': [
      { type: 'required', message: 'la ubicacion es requerido' }
    ],
  };

  ngOnInit() {
    this.getData();

  }

  getData() {
    this.route.data.subscribe(routeData => {
     let data = routeData['data'];
     if (data) {
       this.item = data;
     }
    });
    this.formGasolinera = this.formBuilder.group({
      Nombre: new FormControl(this.item.nombre, Validators.required),
      Ubicacion: new FormControl(this.item.ubicacion, Validators.required)
    });
  }
  closeInventario() {
    this.router.navigate(['/gasolinera']);
  }

  GUARDAREDITGASOLINERA(value) {
    const datos = {
      nombre: value.Nombre,
      ubicacion: value.Ubicacion,
      usuario: localStorage.getItem('idUsuario'),
      tipo: 2,
      estado: 0
    };
    this.mactulizar.update_gasolinera(this.item.id, datos).then(
      res => {
        this.toastController.create({
          message: 'Actualizado correctamente',
          duration: 2000
        }).then((toastData) => {
          toastData.present().then(() => {
            this.router.navigate(['/gasolinera']);
          });
        });
      }
    ).catch(er => {
      this.authService.error2(er);
    });
  }

}
