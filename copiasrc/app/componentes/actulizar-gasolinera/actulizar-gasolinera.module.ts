import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActulizarGasolineraPage } from './actulizar-gasolinera.page';
import { GasolineraResolver } from './gasolinera.Resolver';

const routes: Routes = [
  {
    path: '',
    component: ActulizarGasolineraPage,
    resolve: {
      data: GasolineraResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActulizarGasolineraPage],
  providers:[GasolineraResolver]
})
export class ActulizarGasolineraPageModule {}
