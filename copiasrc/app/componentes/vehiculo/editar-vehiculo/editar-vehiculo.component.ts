import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { EditVehiculoService } from '../../../servicios/edit-vehiculo.service';
import { Validators,ReactiveFormsModule ,FormBuilder, FormGroup, FormControl } from '@angular/forms';



@Component({
  selector: 'app-editar-vehiculo',
  templateUrl: './editar-vehiculo.component.html',
  styleUrls: ['./editar-vehiculo.component.scss'],
})

export class EditarVehiculoComponent implements OnInit {
  placa: string;
  vehilis: any = [];
  /*gaslis: any = ;
  klcambioaceite2: any;
  nomvehi2: any;
  marca2: any;
  cilindraje2: any;
  modelo2: any;
  matricula2: any;
  polizaseguro2: any;
  klvehiculo2: any;*/
  gasolina2: any[];
  idusuario: string;
  seltipogas: any;
  idvehiculo: string;
  validations_form2: FormGroup;

  constructor(
    private modal: ModalController,
    private navparams: NavParams,
    private AFauth: AngularFireAuth,
    private editvehiculoservice: EditVehiculoService,
    private formBuilder: FormBuilder,
    ) { }

    validation_messages = {
      'nomvehi2': [
        { type: 'required', message: 'El nombre es requerido' }
      ],
      'marca2': [
        { type: 'required', message: 'La placa es requerido' }
      ],
      'cilindraje2': [
        { type: 'required', message: 'La marca es requerido' },
      ],
      'modelo2': [
        { type: 'required', message: 'El cilindraje es requerido' }
      ],
      'matricula2': [
        { type: 'areEqual', message: 'EL modelo es requerido' }
      ],
      'polizaseguro2': [
        { type: 'pattern', message: 'La matricula es requerida' }
      ],
      'klcambioaceite2': [
        { type: 'pattern', message: 'La poliza de seguro es requerida' }
      ],
      'klvehiculo2': [
        { type: 'pattern', message: 'Requerido' }
      ],
      'gasolina2': [
        { type: 'pattern', message: 'Requerido' }
      ],
    };


    ngOnInit() {
        this.LISTARGAZOLINERAS2();
        return new Promise<any>((resolve, rejected) => {
          this.placa = this.navparams.get('placa');
          this.editvehiculoservice.read_vehiculo(this.placa).subscribe( data => {
          this.vehilis = data;
          data.map(item => {
            this.idvehiculo = item.id;
            this.validations_form2 = this.formBuilder.group({
              nomvehi2: new FormControl(item.nombre, Validators.required),
              marca2: new FormControl(item.marca, Validators.required),
              cilindraje2: new FormControl(item.cilindraje, Validators.required),
              modelo2: new FormControl(item.modelo, Validators.required),
              matricula2: new FormControl(item.matricula, Validators.required),
              polizaseguro2: new FormControl(item.polizaseguro, Validators.required),
              klcambioaceite2: new FormControl(item.klcambioaceite),
              klvehiculo2: new FormControl(item.klvehiculo),
              gasolina2: new FormControl(item.gasolina),
              placa2: new FormControl(this.placa, Validators.required),
            });
            /*
            this.klcambioaceite2 = item.klcambioaceite;
            this.nomvehi2 = item.nombre;
            this.marca2 = item.marca;
            this.cilindraje2 = item.cilindraje;
            this.modelo2 = item.modelo;
            this.matricula2 = item.matricula;
            this.polizaseguro2 = item.polizaseguro;
            this.klvehiculo2 = item.klvehiculo;
            this.seltipogas = item.gasolina;*/
          });
        });
      });
    }
    closeEditi() {
    this.modal.dismiss();
    }
    EDIGUARDAR(value) {
      return new Promise<any>((resolve, rejected) => {
        this.AFauth.user.subscribe(currentuser => {
            this.editvehiculoservice.update_Student(
              this.idvehiculo,
              value.klcambioaceite2,
              value.cilindraje2,
              value.gasolina2,
              value.klvehiculo2 ,
              value.marca2,
              value.matricula2,
              value.modelo2,
              value.nomvehi2,
              value.polizaseguro2
            );
        });
      });
    }
    LISTARGAZOLINERAS2() {
      return new Promise<any>((resolve, rejected) => {
        this.AFauth.user.subscribe(currentuser => {
            this.idusuario = currentuser.uid;
            this.editvehiculoservice.read_Gazolinera(this.idusuario).subscribe( gas => {
            this.gasolina2 = gas;
            console.log(this.gasolina2);
          });
        });
      });
    }
  }

