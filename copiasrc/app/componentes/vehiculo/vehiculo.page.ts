import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { VehiculoService} from '../../servicios/vehiculo.service';
import { AuthService } from '../../servicios/auth.service';
import { ModalController } from '@ionic/angular';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { message } from './../../models/message';



@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.page.html',
  styleUrls: ['./vehiculo.page.scss'],
})


export class VehiculoPage implements OnInit {

  constructor(
    private router: Router,
    private AFauth: AngularFireAuth,
    private meVehiculo: VehiculoService,
    private authservice: AuthService,
    private modal: ModalController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    private crop: Crop,
    private file: File,
    private webview: WebView,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    private toastController: ToastController,
  ) { }

  public anomax: any  = new Date().getFullYear()+1;
  public anomin: any  = new Date().getFullYear()-20;
  public nomvehi: string;
  public marca: string;
  public cilindraje: string;
  public modelo: string;
  public matricula: string;
  public polizaseguro: string;
  public klcambioaceite: string;
  public klvehiculo: string;
  public gasolina: string;
  public idusuario: string;
  public escoger: string;
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  public info1: boolean  = true;
  public info2: boolean  = false;
  public vehiList: any = [];
  public placa: string;
  public tipo: any;
  image: any;
  public lisgasoli: any = [];
  validations_form: FormGroup;
  automaticClose = false;

  // tslint:disable-next-line: variable-name
  validation_messages = {
    'nomvehi': [
      { type: 'required', message: 'El nombre es requerido' },
    ],
    'marca': [
      { type: 'required', message: 'La marca es requerido' },
    ],
    'cilindraje': [
      { type: 'required', message: 'El cilindraje es requerido' }
    ],
    'modelo': [
      { type: 'areEqual', message: 'EL modelo es requerido' }
    ],
  };
  ngOnInit() {
    this.LISTARGAZOLINERAS();
    this.CARGARVEHICULOS();
    this.resetFields();
    
  }
  toggleSection(index){
    this.vehiList[index].open = !this.vehiList[index].open;
    if(this.automaticClose && this.vehiList[index].open){
      this.vehiList
      .filter((item,itemIndex)=> itemIndex!=index)
      .map(item=> item.open = false);
    }
  }
  GUARDARVEHICULO(value) {
    let klvehi = value.klvehiculo.replace('.', '');
    klvehi = klvehi.replace('.', '');
    klvehi = klvehi.replace('.', '');
    klvehi = klvehi.replace('.', '');


    let klcambio = value.klcambioaceite.replace('.', '');
    klcambio = klcambio.replace('.', '');
    klcambio = klcambio.replace('.', '');
    klcambio = klcambio.replace('.', '');

    let cili = value.cilindraje.replace('.', '');
    cili = cili.replace('.', '');
    cili = cili.replace('.', '');
    cili = cili.replace('.', '');

    let datos = {
      nombre: value.nomvehi,
      marca: value.marca,
      cilindraje: parseInt(cili),
      modelo: new Date(value.modelo).getFullYear(),
      matricula: value.matricula,
      polizaseguro: value.polizaseguro,
      kmcambioaceite: parseInt(klcambio),
      kmvehiculo: parseInt(klvehi),
      usuario: localStorage.getItem('idUsuario'),
      placa: value.placa,
      estado: 0,
      imagen: this.image,
      kmfinal: parseInt(klvehi),
      tipo: value.tipo
    };

    this.meVehiculo.create_Vehiculo(datos)
    .then(
      res => {
        this.resetFields();
        this.toastController.create({
          message: 'Guardado correctamente',
          duration: 2000
        }).then((toastData) => {
          toastData.present().then(() => {
            this.router.navigate(['/home-results']);
          });
        });
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }
  segmentChanged(ev: string) {
    // tslint:disable-next-line: triple-equals
    if (ev == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }
  masinfo(op1,op2) {
    console.log(op1,op2);
    this.info1 = op1;
    this.info2 = op2;
  }
  CARGARVEHICULOS() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meVehiculo.read_Vehiculos(this.idusuario).subscribe( vehi => {
          this.vehiList = vehi;
          console.log(this.vehiList);
        });
      });
    });
  }
  GUARDARVEHICULO1(idvheiculo: string) {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meVehiculo. update_VhiculoSel(this.idusuario, idvheiculo);
      });
    });
  }

  async ELIMINARVEHICULO(idvheiculo: string) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar el vehículo seleccionado!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ACEPTAR',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.AFauth.user.subscribe(currentuser => {
                  this.idusuario = currentuser.uid;
                  this.meVehiculo. delete_Vehiculo(this.idusuario, idvheiculo);
              });
            });

          }
        }
      ]
    });
    await alert.present();
  }
  EDITARVEHICULO(placa: string, id: string) {
    this.router.navigate(['/actualizar-vehiculo', placa, id]);
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccionar Recurso',
      buttons: [{
        text: 'Mostrar galeria',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Usar Camara',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  pickImage(sourceType) {
    const options: CameraOptions = {
    quality: 100,
    sourceType: sourceType,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.cropImage(imageData);
      }, (err) => {
      // Handle error
      }
      );
    }
    cropImage(fileUrl) {
      this.crop.crop(fileUrl, { quality: 50 })
        .then(
          newPath => {
            this.showCroppedImage(newPath.split('?')[0]);
          },
          error => {
            this.authservice.error2('Error cropping image' + error);
          }
        );
      }
      showCroppedImage(ImagePath) {
        //this.isLoading = true;
        var copyPath = ImagePath;
        var splitPath = copyPath.split('/');
        var imageName = splitPath[splitPath.length - 1];
        var filePath = ImagePath.split(imageName)[0];
        this.file.readAsDataURL(filePath, imageName).then(base64 => {
          // this.croppedImagepath = base64;//this.image
          // this.isLoading = false;
          this.uploadImageToFirebase(base64);
        }, error => {
          this.authservice.error2('Error in showing image' + error);
          // this.isLoading = false;
        });
      }
      async uploadImageToFirebase(image) {
        const loading = await this.loadingCtrl.create({
          message: 'Espere por favor...'
        });
        const toast = await this.toastCtrl.create({
          message: 'Imagen actualizada correctamente',
          duration: 3000
        });
        this.presentLoading(loading);
        let image_src = this.webview.convertFileSrc(image);
        let randomId = Math.random().toString(36).substr(2, 5);
        // uploads img to firebase storage
        this.authservice.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err => {
          console.log(err);
        });
      }
  async presentLoading(loading) {
    return await loading.present();
  }
  resetFields() {
    this.image = './assets/img/imgvehiculo.png';
    this.validations_form = this.formBuilder.group({
      nomvehi: new FormControl(''),
      marca: new FormControl('', Validators.required),
      cilindraje: new FormControl('', Validators.required),
      modelo: new FormControl('', Validators.required),
      matricula: new FormControl(''),
      polizaseguro: new FormControl(''),
      klcambioaceite: new FormControl(''),
      klvehiculo: new FormControl(''),
      placa: new FormControl(''),
      tipo: new FormControl('m', Validators.required),
    });
  }
  LISTARGAZOLINERAS() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meVehiculo.read_Gazolinera(this.idusuario).subscribe( gas => {
          this.lisgasoli = gas;
        });
      });
    });
  }
}
