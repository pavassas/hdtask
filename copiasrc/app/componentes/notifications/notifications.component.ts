import { Component, OnInit } from '@angular/core';
import { NotificationsComponentServiceService } from 'src/app/servicios/notifications-component-service.service';
import { AlertController } from '@ionic/angular';
import { RecordatorioService } from 'src/app/servicios/recordatorio.service';
import { RegistroService } from './../../servicios/registro.service';
import { AuthService } from './../../servicios/auth.service';
// import undefined = require('firebase/empty-import');

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  public datos: any = [];
  automaticClose = false;
  listDetalle$: any = [];
  constructor(
    private servinotifi: NotificationsComponentServiceService,
    public alertController: AlertController,
    public mRecor: RecordatorioService,
    public registroservice: RegistroService,
    public authservice: AuthService
    ) { }

  ngOnInit() {

    this.authservice.getDetalle(localStorage.getItem('opDetalle'),localStorage.getItem('IdTask'),localStorage.getItem('idusuario')).subscribe( (data:any[])=>{
      this.listDetalle$ = JSON.parse(JSON.stringify(data));
      //TASK = JSON.parse(JSON.stringify(data));
      console.log(this.listDetalle$);
    },(error)=>{
      console.log(error);
    })


     /* return new Promise<any>((resolve, rejected) => {
      this.servinotifi.read_Alerta().subscribe( reg => {
        this.datos = reg;
      });
    });*/
  }
  verAlerta(index) {
    this.datos[index].open = !this.datos[index].open;
    if (this.automaticClose && this.datos[index].open) {
      this.datos
      // tslint:disable-next-line: triple-equals
      .filter((item, itemIndex) => itemIndex != index)
      .map(item => item.open = false);
    }
  }
  async ELIMINARECORDATORIO(id) {
    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: 'Deseas eliminar recordatorio!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.mRecor.delete_recordatorio(id);
            });
          }
        }
      ]
    });
    await alert.present();

  }
  async RECORDATORIOREALIZADO(idvehiculo, categoria, tip , nom, km, fec, id, rep, mes, idn) {

    const fechaactual: any = new Date();

    if (fec == '' || fec == undefined || fec == null) {
      fec = fechaactual;
    }

    const tipo = {
      cat : categoria,
      ck: true,
      nombre: nom,
      tipo: tip,
      valor: 0,
    };
    const datos = {
      idvehiculo: idvehiculo,
      fecha: fec,
      // local:value.sellocal,
      local:'',
      categoria: categoria,
      nota:'Ejecucion de recordatorio',
      detalle: [tipo],
      km:  km,
      idrecordatorio: id
    };
    console.log(datos);

    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: 'Deseas cambiar el estado del recordatorio!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
               this.registroservice.ejecutar_Registro(datos, rep, mes, idn);
            });
          }
        }
      ]
    });
    await alert.present();

  }

}
