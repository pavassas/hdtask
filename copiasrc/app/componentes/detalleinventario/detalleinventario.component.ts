import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { DetalleinventarioService } from '../../servicios/detalleinventario.service';
import { AuthService } from '../../servicios/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-detalleinventario',
  templateUrl: './detalleinventario.component.html',
  styleUrls: ['./detalleinventario.component.scss'],
})
export class DetalleinventarioComponent implements OnInit {
  public idinv: string;
  public detinvlis: any = [];
  public  marca: string;
  public  almacen: string;
  public  fecha: string;
  public  cantidad: number;
  public  valor: string;
  public  candisponible: string;
  constructor(
    private modal: ModalController,
    private navparams: NavParams,
    private meDetInve: DetalleinventarioService,
    private authservice: AuthService,
    private AFauth: AngularFireAuth
  ) { }

  ngOnInit() {
      return new Promise<any>((resolve, rejected) => {
        this.idinv = this.navparams.get('idinven');
        this.candisponible = this.navparams.get('candisponible');
        this.meDetInve.LisdetaInven(this.idinv ).subscribe( deta => {
        this.detinvlis = deta;
      });
    });
  }

  closeInventario() {
    this.modal.dismiss();
  }
  GUARDARDETALLEINVENT() {
    this.idinv = this.navparams.get('idinven');/*
    return new Promise<any>((resolve, rejected) => {
        // tslint:disable-next-line: max-line-length
        this.meDetInve.guardarDetalleinve(
          this.idinv,
          this.marca,
          this.almacen,
          this.fecha,
          this.cantidad,
          this.valor
        );
    });*/
  }
}
