import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { InventarioService } from 'src/app/servicios/inventario.service';

@Injectable()
export class InventarioResolver implements Resolve<any> {

  constructor(

    private meInventeraio: InventarioService
      ) {}

      resolve(route: ActivatedRouteSnapshot) {
        return new Promise((resolve, reject) => {
          let itemId = route.paramMap.get('id');
          
          this.meInventeraio.edit_inventario(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          })
        })
      }
}
