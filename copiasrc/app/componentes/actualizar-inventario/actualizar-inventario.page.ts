import { Component, OnInit } from '@angular/core';
import { InventarioService } from 'src/app/servicios/inventario.service';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../servicios/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-actualizar-inventario',
  templateUrl: './actualizar-inventario.page.html',
  styleUrls: ['./actualizar-inventario.page.scss'],
})
export class ActualizarInventarioPage implements OnInit {
  item: any;
  formInventario: FormGroup;

  constructor(
    private meInventeraio: InventarioService,
    public toastController: ToastController,
    private router: Router,
    private authService: AuthService,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
  ) { }
  validation_messages = {
    'nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
  };


  ngOnInit() {
    this.getData();
  }
  getData() {
    this.route.data.subscribe(routeData => {
     let data = routeData['data'];
     if (data) {
       this.item = data;
     }
    });
    this.formInventario = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, Validators.required),
    });
  }
  GUARDAREDICIONINVENTARIO(values) {
    const datos = {
      disponible: this.item.disponible,
      entradas: this.item.entradas,
      estados: 0,
      idvehi: localStorage.getItem('idVehiculo'),
      nombre: values.nombre,
      salidas: this.item.salidas
    };
    this.meInventeraio.update_inventario(this.item.id, datos).then(
      res => {
        this.toastController.create({
          message: 'Actualizado correctamente',
          duration: 2000
        }).then((toastData) => {
          toastData.present().then(() => {
            this.router.navigate(['/inventario']);
          });
        });
      }
    ).catch(er => {
      this.authService.error2(er);
    });
  }
}
