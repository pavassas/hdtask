import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActualizarInventarioPage } from './actualizar-inventario.page';
import{ InventarioResolver } from './actulizar-inventario.resolver';

const routes: Routes = [
  {
    path: '',
    component: ActualizarInventarioPage,
    resolve: {
      data: InventarioResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarInventarioPage],
  providers: [InventarioResolver]
})
export class ActualizarInventarioPageModule {}
