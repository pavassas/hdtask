import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InventarioPage } from './inventario.page';

import {NgxMaskIonicModule} from 'ngx-mask-ionic';

const routes: Routes = [
  {
    path: '',
    component: InventarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskIonicModule
  ],
  declarations: [InventarioPage]
})
export class InventarioPageModule {}
