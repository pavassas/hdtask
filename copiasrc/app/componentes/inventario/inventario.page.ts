import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {InventarioService } from '../../servicios/inventario.service';
import { ModalController } from '@ionic/angular';
import { AuthService } from '../../servicios/auth.service';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.page.html',
  styleUrls: ['./inventario.page.scss'],
})
export class InventarioPage implements OnInit {
  public nombre: string;
  public cantidad: string;
  public idusuario: string;
  public valor: string;
  public  gasoList: any = [];
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  public vehilis: any = [];
  public inveLis: any = [];
  formInventario: FormGroup;
  public vehiculoList: any = [];
  mivehiculo: any;
  mivehiculo2: any = localStorage.getItem('idVehiculo'); 

  constructor(
    private AFauth: AngularFireAuth,
    private meInventeraio: InventarioService,
    private modal : ModalController,
    private authservice: AuthService,
    public vehiculoservice: VehiculoService,
    public alertController: AlertController,

    // tslint:disable-next-line: no-shadowed-variable
    public formBuilder: FormBuilder,
    private router: Router,

  ) { }
  validation_messages = {
    'nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],

    'fecha':[
      {type: '', message: 'Requerido'}
    ],
    'valor':[
      {type: '', message: 'Requerido'}
    ],
    'cantidad':[
      {type: '', message: 'Requerido'}
    ],
  };


  ngOnInit() {
    this.mivehiculo = localStorage.getItem('idVehiculo');
    this.vehiculoservice.getVehiculos(localStorage.getItem('idUsuario')).subscribe( veh => {
      this.vehiculoList = veh;
      console.log(this.vehiculoList);
    });

    this.meInventeraio.LisInvent(this.mivehiculo2).subscribe( inven => { this.inveLis = inven; } );
    this.CAMPOS();
  }
  
  GUARDARINVENTARIO(value) {
    const fecinven = new Date(value.fecha);
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
        this.idusuario = currentuser.uid;
        this.meInventeraio.GuardarInventario(
          value.nombre,
          value.marca,
          value.almacen,
          fecinven,
          value.valor,
          value.cantidad,
          this.idusuario).subscribe( combu => {
            this.RESETCAMPOS();
            this.segmentChanged('Lista');
        });
      });
    });
  }
  LISTARINVENTARIO() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meInventeraio.LisInvent(this.mivehiculo2).subscribe( inven => {
          this.inveLis = inven;
        });
      });
    });
  }
  segmentChanged(ev: string) {
    // tslint:disable-next-line: triple-equals
    if (ev == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }
  REGISTARENTRADA(idinv, salidas, entradas) {
    this.router.navigate(['/entradas-inventario', idinv, salidas, entradas]);
    }

    REGISTRARSALIDA(idinv, disponible) {
      this.router.navigate(['/salida-inventario', idinv, disponible]);
    }
    EDITARINVENTARIO(idinv) {
      this.router.navigate(['/actualizar-inventario', idinv]);
    }
    CAMPOS() {
      this.formInventario = this.formBuilder.group({
        nombre: new FormControl('', Validators.required),
        marca: new FormControl(''),
        almacen: new FormControl(''),
        fecha: new FormControl(new Date().toISOString(), Validators.required),
        valor: new FormControl('', Validators.required),
        cantidad: new FormControl('', Validators.required),
      });
    }
    changeVehiculo() {
      this.mivehiculo2 = this.mivehiculo
      this.ngOnInit();
      localStorage.setItem('idVehiculo', this.mivehiculo);
      this.authservice.idvehisel = this.mivehiculo;
    }





    async ELIMINARARTICULO(id: string) {
      const alert = await this.alertController.create({
        header: 'Confirmación!',
        message: 'Deseas eliminar articulo!!!',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
            }
          }, {
            text: 'ACEPTAR',
            handler: () => {
              return new Promise<any>((resolve, rejected) => {
                this.meInventeraio.Eliminar_inventario( id );
              });
            }
          }
        ]
      });
      await alert.present();
    }

    RESETCAMPOS() {
      this.formInventario = this.formBuilder.group({
        nombre: new FormControl(''),
        marca: new FormControl(''),
        almacen: new FormControl(''),
        fecha: new FormControl(new Date().toISOString()),
        valor: new FormControl(''),
        cantidad: new FormControl(''),
      });
    }
}
