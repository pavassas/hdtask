import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import {AbastecimientoService} from '../../servicios/abastecimiento.service';




@Component({
  selector: 'app-abastecimiento',
  templateUrl: './abastecimiento.page.html',
  styleUrls: ['./abastecimiento.page.scss'],
})
export class AbastecimientoPage implements OnInit {
  customPickerOptions: any;
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  formAbas: FormGroup;
  public idusuario: string;
  public lisgasoli: any = [];
  public abasteList: any = [];
  public lislocalgaso: any = [];
  public gasdefault: string;
  constructor(
    private router: Router,
    public actionSheetController: ActionSheetController,
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    private AFauth: AngularFireAuth,
    private mAbaste: AbastecimientoService,
    public alertController: AlertController,
  ) {}
  validation_messages = {
    'Fecha': [
      { type: 'required', message: 'Requerido' }
    ],
    'Kilometro': [
      { type: 'required', message: 'Requerido' }
    ],
    'Hora': [
      { type: 'required', message: 'Requerido' }
    ],
    'Gasolina': [
      { type: 'required', message: 'Requerido' }
    ],

    'Precio': [
      { type: 'required', message: 'Requerido' }
    ],

    'Galon':[
      { type: 'required', message: 'Requerido' }
    ],
    'Gasolinera': [
      { type: 'required', message: 'Requerido' }
    ],
    'Nota': [
      { type: 'required', message: 'Requerido' }
    ],
  };

  Onlogout() {
    this.authservice.logout();
  }
  ngOnInit() {
    this.LISTARGAZOLINERAS();
    this.LISTARLOCALGASOLINA();
    this.LISTABASTESIMIENTO();
    this.mAbaste.read_tipogas(localStorage.getItem('idVehiculo')).subscribe( abaste => {
      console.log(abaste);
      this.gasdefault = abaste.gasolina;
      this.formAbas = this.formBuilder.group({
        Fecha: new FormControl(new Date().toISOString(), Validators.required),
        Kilometro: new FormControl('', Validators.required),
        Hora: new FormControl('', Validators.required),
        Gasolina: new FormControl(abaste.gasolina, Validators.required),
        Precio: new FormControl('', Validators.required),
        Galon: new FormControl('', Validators.required),
        Total: new FormControl(''),
        Gasolinera: new FormControl('', Validators.required),
        Nota: new FormControl('', Validators.required),
      });
    });
    this.reset_registro();
  }
  reset_registro(){
    this.formAbas = this.formBuilder.group({
      Fecha: new FormControl(new Date().toISOString(), Validators.required),
      Kilometro: new FormControl('', Validators.required),
      Hora: new FormControl('', Validators.required),
      Gasolina: new FormControl(this.gasdefault, Validators.required),
      Precio: new FormControl('', Validators.required),
      Galon: new FormControl('', Validators.required),
      Total: new FormControl(''),
      Gasolinera: new FormControl('', Validators.required),
      Nota: new FormControl('', Validators.required),
    });
  }

  segmentChanged(ev: any) {
    // tslint:disable-next-line: triple-equals
    if (ev.detail.value == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }
  LISTARGAZOLINERAS() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.mAbaste.read_Gazolinera(this.idusuario).subscribe( gas => {
          this.lisgasoli = gas;
        });
      });
    });
  }
  LISTARLOCALGASOLINA() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.mAbaste.read_loclagazolina(this.idusuario).subscribe( lgas => {
          this.lislocalgaso = lgas;
        });
      });
    });
  }
  /*GUARDARABASTE(value) {
    // tslint:disable-next-line: no-unused-expression
    new Promise<any>((resolve, rejected) => {
    this.AFauth.user.subscribe(currentuser => {
      this.idusuario = currentuser.uid;
      this.mAbaste.create_Abaste(
        value.Fecha,
        value.Kilometro,
        value.Hora,
        value.Gasolina,
        value.Precio,
        value.Galon,
        value.Gasolinera,
        value.Nota,
        value.Total,
        localStorage.getItem('idVehiculo')
        );
    });
    this.reset_registro();
  });
  }*/
  LISTABASTESIMIENTO() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.mAbaste.read_Abastecimiento().subscribe( abaste => {
          this.abasteList = abaste;
          console.log(abaste);
        });
      });
    });
  }
  async ELIMINARABASTE(idabastecimiento) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar el abastecimiento!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.AFauth.user.subscribe(currentuser => {
                  this.idusuario = currentuser.uid;
                  this.mAbaste. delete_Abastecimiento(idabastecimiento);
              });
            });

          }
        }
      ]
    });
    await alert.present();
  }
  EDITARABASTE(id: string) {
    this.router.navigate(['/actualizar-abastecimiento/', id]);
  }

  GUARDARABASTE(value) {
    let datos = {
      fecha: value.Fecha,
      kilometro: value.Kilometro,
      hora: value.Hora,
      gasolina: value.Gasolina,
      precio: value.Precio,
      galon: value.Galon,
      gasolinera: value.Gasolinera,
      nota: value.Nota,
      idvehiculo: localStorage.getItem('idVehiculo'),
      estado: 0,
      Total: value.Total
    };
    this.mAbaste.create_abastecimiento(datos)
    .then(
      res => {
        this.authservice.ok2('Abastecimiento guardado correctamente');
        this.reset_registro();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }
}

