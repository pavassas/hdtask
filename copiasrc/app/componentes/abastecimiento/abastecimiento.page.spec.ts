import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbastecimientoPage } from './abastecimiento.page';

describe('AbastecimientoPage', () => {
  let component: AbastecimientoPage;
  let fixture: ComponentFixture<AbastecimientoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbastecimientoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbastecimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
