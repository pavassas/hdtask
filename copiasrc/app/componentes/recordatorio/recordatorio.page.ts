import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import {CategoriaService } from '../../servicios/categoria.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RecordatorioService } from 'src/app/servicios/recordatorio.service';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
// import undefined = require('firebase/empty-import');

@Component({
  selector: 'app-recordatorio',
  templateUrl: './recordatorio.page.html',
  styleUrls: ['./recordatorio.page.scss'],
})
export class RecordatorioPage implements OnInit {
  public cateList: any = [];
  public selcate: string;
  public tipoList: any = [];
  formRecordatorio: FormGroup;
  public repi1: boolean  = false;
  public repi2: boolean  = true;
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  public selrepi: string;
  public mece: any = [];
  public recordatorioList: any = [];
  public formreg:boolean = false;
  public formlis:boolean = true;
  public vehiculoList: any = [];

  // valores de input
  public inputDisabledmk: boolean = true;
  public inputDisabledfec: boolean = true;
  public inputDisabledmes: boolean = true;


  public chkm: boolean = false;
  public chfecha1: boolean = false;
  public chtiempo: boolean = false;

  public km: any ; 

 
  // datos filtro
  mivehiculo: any;

  automaticClose = false;

  constructor(

    private authservice: AuthService,
    public cate: CategoriaService,
    public formBuilder: FormBuilder,
    public mRecor: RecordatorioService,
    public alertController: AlertController,
    private router: Router,
    private localNotifications: LocalNotifications,
    private vehiculoservice: VehiculoService

    ) { }
    validation_messages = {
      'selcate': [
        { type: 'required', message: 'requerido' }
      ],
      'seltipo': [
        { type: 'required', message: 'requerido' }
      ],
      'repetir': [
        { type: 'required', message: 'requerido' }
      ],
      'titulo': [
        {type: 'required', message: 'requerido'}
      ],

    };

  ngOnInit() {
    this.vehiculoservice.getVehiculos(localStorage.getItem('idUsuario')).subscribe( veh => {
      this.vehiculoList = veh;
    });
    this.MESES();
    this.LISTARRECORDATORIO('vacio');
    this.cate.getCategorias().subscribe( cat => {
      this.cateList = cat;
      // console.log(cat)
    });
    this.resetRecordatorio();
    this.mRecor.count_recordatorio();
  }
  opcion(op: string) {
    // tslint:disable-next-line: triple-equals
    if (op == 'nuevo') {
      this.ocultar1 = true;
      this.ocultar2 = false;
    } else {
      this.ocultar1 = false;
      this.ocultar2 = true;
    }
  }
  changeVehiculo() {
    localStorage.setItem('idVehiculo', this.mivehiculo);
    this.authservice.idvehisel = this.mivehiculo;
    this.LISTARRECORDATORIO('vacio');
  }

  resetRecordatorio() {
     this.mivehiculo = localStorage.getItem('idVehiculo');
     this.formRecordatorio = this.formBuilder.group({
      selcate: new FormControl('', Validators.required),
      seltipo: new FormControl('', Validators.required),
      repetir: new FormControl(1, Validators.required),
      titulo: new FormControl('', Validators.required),
      kmetro: new FormControl(''),
      fecha: new FormControl(new Date().toISOString()),
      mes: new FormControl(''),
      km: new FormControl(false),
      fecha1: new FormControl(false),
      tiempo: new FormControl(false),
    });
     this.repi1 = true;
     this.repi2 = false;
  }
  toggleOption(opc: boolean, opc2: boolean) {
    console.log('Entro por aca');
    this.ocultar1 = opc2;
    this.ocultar2 = opc;
    this.formlis = opc;
    this.formreg = opc2;
  }
  cargartipo() {
    this.cate.getTipos(this.formRecordatorio.get('selcate').value).subscribe( tip => {
      this.tipoList = tip;
    });
  }
  segmentChanged(ev: any) {
    this.selrepi = this.formRecordatorio.get('repetir').value;
    // tslint:disable-next-line: triple-equals
    if (this.formRecordatorio.get('repetir').value == 1) {
    this.repi1 = true;
    this.repi2 = false;
    } else {
    this.repi1 = false;
    this.repi2 = true;
    }
  }

  MESES() {
    for (let v = 1; v <= 60; v++ ) {
      this.mece.push(v);
    }
  }
  /*segmentChanged2(ev: any) {
    // tslint:disable-next-line: triple-equals
    if (ev.detail.value == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }*/

  LISTARRECORDATORIO(ev: any) {
    let tip;
    // tslint:disable-next-line: triple-equals
    if (ev == 'vacio') {
      tip = '';
    } else {
      tip = ev.detail.value;
    }
    console.log(tip);
    return new Promise<any>((resolve, rejected) => {
        this.mRecor.listar_recordatorio(tip).subscribe( record => {
        this.recordatorioList = record;
        console.log(this.recordatorioList);
      });
    });
  }

  async ELIMINARRECORDATORIO(id: string) {
    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: 'Deseas eliminar recordatorio!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ACEPTAR',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.mRecor.delete_recordatorio(id);
            });
          }
        }
      ]
    });
    await alert.present();

  }

  GUARDARRECORDATORIO(values) {

    let fechaseleccionada: any ;
    // tslint:disable-next-line: max-line-length
    if (localStorage.getItem('idVehiculo') == '' || localStorage.getItem('idVehiculo') == undefined || localStorage.getItem('idVehiculo') == null) {
      this.authservice.error2('Deve seleccionar el vehiculo');
    } else {
 
    // tslint:disable-next-line: max-line-length
    if ((this.chkm == false ) && (this.chfecha1 == false )&&(this.chtiempo == false)){
      this.authservice.error2('Debes de seleccionar una opción métrica de recordatorio');
    } else {
      let fechaseleccionada: any

      if (!this.chkm) { values.kmetro = 0; } else { }

      // tslint:disable-next-line: max-line-length
      if (this.chfecha1 == false && parseInt( values.repetir) == 1) {fechaseleccionada = ''; } else if (parseInt( values.repetir) == 1 ) {
        fechaseleccionada = new Date(values.fecha);
        this.chtiempo = false;
        values.mes = '';
      }
  
      if (this.chtiempo == false &&  parseInt( values.repetir) == 2) { } else if (parseInt( values.repetir) == 2) {
        this.chfecha1 = false;
        const fecha = new Date();
        const fechaseleccionada2 = fecha.setMonth(fecha.getMonth() + parseInt(values.mes));
        fechaseleccionada = new Date(fechaseleccionada2);
      }

      if (this.chtiempo == false && this.chfecha1 == false ){values.fecha = ''}
  
      const datos = {
          titulo: values.titulo,
          selcate: values.selcate,
          seltipo: values.seltipo,
          repetir: values.repetir,
          fecha: fechaseleccionada ,
          mes: values.mes,
          km: this.chkm,
          fecha1: this.chfecha1,
          tiempo: this.chtiempo,
          estado: 0,
          // tslint:disable-next-line: radix
          kmetro: parseInt(values.kmetro),
          idvehiculo: localStorage.getItem('idVehiculo'),
          notificacion: 0,
          usuario: localStorage.getItem('idUsuario'),
          idnotificacion: this.mRecor.counta,
          idrecordatorio: 0
        };
      this.mRecor.create_recordatorio(datos)
        .then(
          () => {
            // tslint:disable-next-line: triple-equals
            if ( fechaseleccionada!= '' && fechaseleccionada != undefined) {
              const ano = new Date(values.fecha).getFullYear();
              const mes = new Date(values.fecha).getMonth();
              const dia = new Date(values.fecha).getDay();
              this.localNotifications.schedule({
                id: this.mRecor.counta,
                title: 'Recordatorio Pavas MotoR',
                text: values.titulo,
                foreground: true,
                trigger: {at: new Date(new Date(fechaseleccionada).getTime() + 3600)},
              });
            }
            this.mRecor.counta = this.mRecor.counta + 1;
            this.authservice.ok2('Recordatorio guardado correctamente');
            this.resetRecordatorio();
          }
        ).catch(err => {
          this.authservice.error2(err);
        });
      }
    }
  }
  ACTULIZARRECORDATORIO(id: string) {
    this.router.navigate(['/actualizar-recordatorio', id]);
  }
  VALIDARTIPO(values) {
    this.inputDisabledfec = values.fecha1 ? false : true;
    this.inputDisabledmk = values.km ? false : true;
    this.inputDisabledmes = values.tiempo ? false : true;


    this.chfecha1 = values.fecha1 ? true : false;
    this.chkm = values.km ? true : false;
    this.chtiempo = values.tiempo ? true : false;
  }

  toggleSection(index) {
    this.recordatorioList[index].open = !this.recordatorioList[index].open;
    if ( this.automaticClose && this.recordatorioList[index].open) {
      this.recordatorioList
      .filter((item, itemIndex) => itemIndex != index)
      .map(item => item.open = false);
    }
  }
}
