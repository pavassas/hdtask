import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ActualizarLocalPage } from './actualizar-local.page';
import { LocalResolver } from './local.Resolver';

const routes: Routes = [
  {
    path: '',
    component: ActualizarLocalPage,
    resolve: {
      data: LocalResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActualizarLocalPage],
  providers: [LocalResolver]
})
export class ActualizarLocalPageModule {}
