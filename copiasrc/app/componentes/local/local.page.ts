import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {LocalesService } from '../../servicios/locales.service';
import { AuthService } from '../../servicios/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-local',
  templateUrl: './local.page.html',
  styleUrls: ['./local.page.scss'],
})
export class LocalPage implements OnInit {
  public Nombre: string;
  public ubicacion: string;
  public idusuario: string;
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  public localesList: any = [];
  formLocal: FormGroup;

  constructor(
    private AFauth: AngularFireAuth,
    private meLocales: LocalesService,
    private authservice: AuthService,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private router: Router,
  ) { }
  validation_messages = {
    'Nombre': [
      { type: 'required', message: 'El nombre es requerido' }
    ],
    'ubicacion': [
      { type: 'required', message: 'La ubicacion es requerido' }
    ],
  };

  ngOnInit() {
    this.LISTARLOCALES();
    this.CAMPOS();

  }
  LISTARLOCALES() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.meLocales.getLocales(this.idusuario).subscribe( local => {
          this.localesList = local;
        });
      });
    });
  }
  segmentChanged(ev: string) {
    // tslint:disable-next-line: triple-equals
    if (ev == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }

  async ELIMINARLOCALES(id: string) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar local!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ACEPTAR',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.meLocales.delete_local(id);
            });
          }
        }
      ]
    });
    await alert.present();
  }
  ACTULIZARLOCALES(id: string) {
    this.router.navigate(['/actualizar-local', id]);
  }
  CAMPOS() {
    this.formLocal = this.formBuilder.group({
      Nombre: new FormControl('', Validators.required),
      Ubicacion: new FormControl('', Validators.required),
    });
  }
  GUARDARLOCAL(value) {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
        let datos = {
          nombre: value.Nombre,
          ubicacion: value.Ubicacion,
          usuario: this.idusuario,
          tipo: 1,
          estado: 0
        };
        this.meLocales.create_local(datos).subscribe( inven => {
          //  this.inveLis = inven;
        });
      });
    });
  }
}
