import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { RegistroService } from '../../servicios/registro.service';

@Injectable()
export class ActualizarRegistroResolver implements Resolve<any> {

  constructor(public registroservice: RegistroService,) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      let itemId = route.paramMap.get('id');
      let op = route.paramMap.get('op');
      this.registroservice.edit_Registro(itemId)
      .then(data => {
        data.id = itemId;
        data.op= op;
        resolve(data);
      }, err => {
        reject(err);
      })
    })
  }
}
