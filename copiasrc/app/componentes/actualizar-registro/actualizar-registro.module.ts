import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActualizarRegistroPage } from './actualizar-registro.page';
import { ActualizarRegistroResolver } from './actualizar-registro.resolve';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

const routes: Routes = [
  {
    path: '',
    component: ActualizarRegistroPage,
    resolve: {
      data: ActualizarRegistroResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [ActualizarRegistroPage],
  providers:[ActualizarRegistroResolver]
})
export class ActualizarRegistroPageModule {}
