import { Component, OnInit } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController, ToastController, ModalController, Events } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoriaService } from '../../servicios/categoria.service';
import { LocalesService } from '../../servicios/locales.service';
import { AuthService } from '../../servicios/auth.service';
import { RegistroService } from '../../servicios/registro.service';
import { TiposSeleccionados } from '../../componentes/registro/tipos.model';
import { DetalletipoComponent } from '../../componentes/detalletipo/detalletipo.component';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-actualizar-registro',
  templateUrl: './actualizar-registro.page.html',
  styleUrls: ['./actualizar-registro.page.scss'],
})

export class ActualizarRegistroPage implements OnInit {
  public id: any;
  public cateList: any = [];
  public tipoList: any = [];
  public localList: any = [];
  public selcate: string;
  public idusuario: string;
  selcat: string;
  total: any;
  tiposeleccionados: Observable<TiposSeleccionados>[];
  public tipos: any = [];
  // funciones para la validacion
  validations_formedit: FormGroup;
  item: any;
  public errorMessage: string = '';
  public dcate: boolean = false;
  public dtipo: boolean = false;
  public vtipo: boolean = true;

  validation_messages = {
    'selfecha': [
      { type: 'required', message: 'La fecha es requerida' }
    ],
    'selcate': [
      { type: 'required', message: 'La categoria es requerida' }
    ],
    'txtnota': [
      { type: 'required', message: 'La nota es requerida' }
    ],
    'sellocal': [
      { type: 'required', message: 'El local es requerido' }
    ],
    'txtlocal': [
      { type: 'required', message: 'El local es requerido' }
    ],
    'txtkm': [
      { type: 'required', message: 'El Kilometraje es requerido' }
    ],
  };
  filteredOptionsLocal: Observable<string[]>;

  constructor(
      public router: Router,
      public cate: CategoriaService,
      public localservice: LocalesService,
      private AFauth: AngularFireAuth,
      private authservice: AuthService,
      private modal: ModalController,
      private formBuilder: FormBuilder,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      public registroservice: RegistroService,
      private activatedRoute: ActivatedRoute,
      private route: ActivatedRoute,
      public events: Events
  ) { }

  ngOnInit() {
    this.authservice.tipos.length = 0;
    this.cate.getCategorias().subscribe( cat => {
      this.cateList = cat;
    });
    this.tiposeleccionados = this.authservice.tipos;
    this.events.subscribe('registro:total', (tot, time) => {
      this.total = tot;
    });
    this.resetRegistro();
  }
  resetRegistro() {
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data;
        console.log('op:' + this.item.op);

        if (this.item.op == 2) {
          this.dcate = true;
          this.dtipo = true;
          this.vtipo = false;
        }
        this.selcat = this.item.categoria;
        this.selcate = this.item.categoria;
        this.item.detalle.forEach(item => {
           // let dat = { cat:item.cat }
           this.authservice.addTipo(item, -1);
           // this.authservice.tipos.push(item);
        });
      }
     });
    const fech = this.item.fecha;
    const fech2 = (fech != '')? this.authservice.transform(fech) : '';

    this.validations_formedit = this.formBuilder.group({
      selfecha: new FormControl(fech2, Validators.required),
      // sellocal: new FormControl(this.item.local, Validators.required),
      txtlocal: new FormControl(this.item.local, Validators.required),
      selcate: new FormControl(this.item.categoria, Validators.required),
      txtnota: new FormControl(this.item.nota, Validators.required),
      txtkm: new FormControl(this.item.km, Validators.required),
    });
    this.cargarlocal();
    this.filteredOptionsLocal = this.validations_formedit.get('txtlocal').valueChanges
    .pipe(
      startWith(''),
      map(value => this._filterLocal(value))
    );
    // const tot = this.tipos.reduce((actual, anterior) =>  parseFloat(actual) + parseFloat(anterior.valor), 0);
   // this.authservice.createTotal(tot);

  }
  private _filterLocal(value: string): string[] {
    const filterValue = value.toString().toLowerCase();
    return this.localList.filter(option => option.nombre.toString().toLowerCase().includes(filterValue));
  }
  openTipos() {
    if (this.selcat != this.selcate) {
        this.authservice.tipos.length = 0;
        this.authservice.createTotal(0);
    }
    this.selcat = this.selcate;
    if (this.selcate) {
        this.modal.create({
        component: DetalletipoComponent,
        componentProps : {
          cat: this.selcate
        }
      }).then( (modal) => modal.present() );
    } else {
      this.authservice.error2('Seleccionar la categoria');
    }
  }
  cargarlocal() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
        this.idusuario = currentuser.uid;
        this.localservice.getLocales(this.idusuario).subscribe( loc => {
          this.localList = loc;
        });
      });
    });
  }

  deleteTip(i) {
    this.authservice.tipos.splice(i, 1);
    const tot = this.tipos.reduce((actual, anterior) =>  parseFloat(actual) + parseFloat(anterior.valor), 0);
    this.authservice.createTotal(tot);
  }

  OnSubmitUpdateRegistro(value) {
     const nui = this.authservice.tipos.length;
     const fec = new Date(value.selfecha);
     if ( nui <= 0) {
       this.authservice.error2('No hay detalle seleccionado');
     } else {
        const datos = {
          fecha: fec,
          // local: value.sellocal,
          local: value.txtlocal,
          categoria: this.selcate,
          nota: value.txtnota,
          detalle: this.authservice.tipos,
          idvehiculo: this.item.idvehiculo,
          km: value.txtkm,
          idrecordatorio: this.item.idrecordatorio
        };
        // console.log(datos);
        this.registroservice.update_Registro(this.item.id, datos).then(res => {
          const datoslocal = {
            nombre: value.txtlocal,
            ubicacion: '',
            usuario: localStorage.getItem('idUsuario'),
            tipo: 1,
            estado: 0
          };
          this.localservice.create_local(datoslocal).subscribe( inven => {
          });
          this.authservice.tipos.length = 0;
          this.authservice.ok2('Registro modificado correctamente');
          this.router.navigate(['/registro']);
        }).catch(err => {
            this.authservice.error2('No se guardo el registro.' + err );
        });
     }
  }
  updTipo(e,i,categoria,tipo,nombre){
    console.log(e.detail.value);
    console.log(i);
    let dat = {
      cat:categoria,
      tipo:tipo,
      nombre:nombre,
      ck:true,
      valor:e.detail.value
    }
   this.authservice.tipos[i].valor = e.detail.value;
    if ( this.authservice.tipos.length > 0) {
    this.authservice.totaltipo  = this.authservice.tipos.reduce((actual, anterior) =>  parseFloat(actual) + parseFloat(anterior.valor), 0);
    this.authservice.createTotal(this.authservice.totaltipo);
  
    // this.totaltipo.next();
  }
    // this.authservice.addTipo(dat,i);
    console.log(this.authservice.tipos[i].valor);
  }

}
