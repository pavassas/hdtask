import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {CombustibleService} from '../../servicios/combustible.service';
import { AuthService } from '../../servicios/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-combustible',
  templateUrl: './combustible.page.html',
  styleUrls: ['./combustible.page.scss'],
})
export class CombustiblePage implements OnInit {
  public Nombre: string;
  public idusuario: string;
  public ocultar1: boolean  = false;
  public ocultar2: boolean  = true;
  public combList: any = [];
  formcombustible: FormGroup;
  
  constructor(
    private AFauth: AngularFireAuth,
    private CombustibleServices: CombustibleService,
    private authservice: AuthService,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private router: Router) { }
    validation_messages = {
      'Nombre': [
        { type: 'required', message: 'El nombre es requerido' }
      ],
    };

  ngOnInit() {
    this.CARGARCOMBUSTIBLE();
    this.CAMPOS();
  }
  segmentChanged(ev: string) {
    // tslint:disable-next-line: triple-equals
    if (ev == 'nuevo') {
    this.ocultar1 = true;
    this.ocultar2 = false;
    } else {
    this.ocultar1 = false;
    this.ocultar2 = true;
    }
  }
  CARGARCOMBUSTIBLE() {
    return new Promise<any>((resolve, rejected) => {
      this.AFauth.user.subscribe(currentuser => {
          this.idusuario = currentuser.uid;
          this.CombustibleServices.getCombustible(this.idusuario).subscribe( combus => {
          this.combList = combus;
        });
      });
    });
  }


  async ELIMINARCOMBUSTIBLE(id: string) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas eliminar el combustible!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.CombustibleServices.delete_combustible(id);
            });
          }
        }
      ]
    });
    await alert.present();

  }
  CAMPOS() {
    this.formcombustible = this.formBuilder.group({
      Nombre: new FormControl('', Validators.required),
    });
  }

  ACTULIZARCOMBUSTIBLE(id: string) {
    this.router.navigate(['/actualizar-combustible', id]);
  }
  GUARDARCOM(value) {
    let datos = {
      nombre: value.Nombre,
      usuario: this.idusuario,
      estado: 0
    };
    this.CombustibleServices.create_combustible(datos)
    .then(
      res => {
        this.authservice.ok2('Combustible guardado correctamente');
        this.CAMPOS();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });
  }
}
