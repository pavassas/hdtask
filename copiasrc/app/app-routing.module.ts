import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NoLoginGuard } from './guards/no-login.guard';

const routes: Routes = [
  { path: '', loadChildren: './componentes/login/login.module#LoginPageModule', canActivate : [NoLoginGuard] },
  { path: 'home-results', loadChildren: './componentes/home-results/home-results.module#HomeResultsPageModule',canActivate : [AuthGuard] },
  { path: 'about', loadChildren: './componentes/about/about.module#AboutPageModule' ,canActivate : [AuthGuard] },
  { path: 'login', loadChildren: './componentes/login/login.module#LoginPageModule', canActivate : [NoLoginGuard] },
  { path: 'settings', loadChildren: './componentes/settings/settings.module#SettingsPageModule' ,canActivate : [AuthGuard] },
  
  { path: 'local', loadChildren: './componentes/local/local.module#LocalPageModule' ,canActivate : [AuthGuard] },
  { path: 'gasolinera', loadChildren: './componentes/gasolinera/gasolinera.module#GasolineraPageModule',canActivate : [AuthGuard]  },
  { path: 'combustible', loadChildren: './componentes/combustible/combustible.module#CombustiblePageModule',canActivate : [AuthGuard]  },
  // tslint:disable-next-line: max-line-length
  { path: 'abastecimiento', loadChildren: './componentes/abastecimiento/abastecimiento.module#AbastecimientoPageModule', canActivate : [AuthGuard]  },
  { path: 'registro', loadChildren: './componentes/registro/registro.module#RegistroPageModule', canActivate : [AuthGuard]  },
  { path: 'inventario', loadChildren: './componentes/inventario/inventario.module#InventarioPageModule', canActivate : [AuthGuard]  },
  { path: 'vehiculo', loadChildren: './componentes/vehiculo/vehiculo.module#VehiculoPageModule', canActivate : [AuthGuard]  },
  // tslint:disable-next-line: max-line-length
  { path: 'recordatorio', loadChildren: './componentes/recordatorio/recordatorio.module#RecordatorioPageModule', canActivate : [AuthGuard]  },
  { path: 'register', loadChildren: './componentes/register/register.module#RegisterPageModule', canActivate : [NoLoginGuard] },
  // tslint:disable-next-line: max-line-length
  { path: 'salida-inventario/:id/:disponible', loadChildren: './componentes/salida-inventario/salida-inventario.module#SalidaInventarioPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'entradas-inventario/:id/:salidas/:entradas', loadChildren: './componentes/entradas-inventario/entradas-inventario.module#EntradasInventarioPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-vehiculo/:placa/:id', loadChildren: './componentes/actualizar-vehiculo/actualizar-vehiculo.module#ActualizarVehiculoPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actulizar-gasolinera/:id', loadChildren: './componentes/actulizar-gasolinera/actulizar-gasolinera.module#ActulizarGasolineraPageModule' },
  { path: 'actualizar-local/:id', loadChildren: './componentes/actualizar-local/actualizar-local.module#ActualizarLocalPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-combustible/:id', loadChildren: './componentes/actualizar-combustible/actualizar-combustible.module#ActualizarCombustiblePageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-abastecimiento/:id', loadChildren: './componentes/actualizar-abastecimiento/actualizar-abastecimiento.module#ActualizarAbastecimientoPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-registro/:id/:op', loadChildren: './componentes/actualizar-registro/actualizar-registro.module#ActualizarRegistroPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-recordatorio/:id', loadChildren: './componentes/actualizar-recordatorio/actualizar-recordatorio.module#ActualizarRecordatorioPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'actualizar-inventario/:id', loadChildren: './componentes/actualizar-inventario/actualizar-inventario.module#ActualizarInventarioPageModule' },
  { path: 'edit-profile', loadChildren: './componentes/edit-profile/edit-profile.module#EditProfilePageModule' ,canActivate : [AuthGuard] },
  

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
