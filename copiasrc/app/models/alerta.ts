export interface Recordatorio {
    estado: number;
    fecha: string;
    fecha1: string;
    idnotificacion: string;
    km: string;
    kmetro: string;
    mes: string;
    notificacion: string;
    repetir: string;
    selcate: string;
    seltipo: string;
    tiempo: string;
    titulo: string;
    idvehiculo: string;
}
export interface Tipo {
    id: string;
    nombre: string;
}
