import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgSelectModule,NgSelectConfig } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
// firebase
import { firebaseConfig } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';

// Modal Pages
import { ImagePageModule } from './componentes/modal/image/image.module';
import { SearchFilterPageModule } from './componentes/modal/search-filter/search-filter.module';

// file/camara
import { File } from '@ionic-native/file/ngx';
import { Camera } from '@ionic-native/Camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';

import { WebView } from '@ionic-native/ionic-webview/ngx';

// Components
import { NotificationsComponent } from './componentes/notifications/notifications.component';
//import { ImageModalPageModule } from './componentes/image-modal/image-modal.module';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';


import { DetalletipoComponent } from './componentes/detalletipo/detalletipo.component';



import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DatePipe } from '@angular/common';
//import { EmailComposer } from '@ionic-native/email-composer/ngx';


//import { HttpModule } from '@angular/http';

//import { FCM } from '@ionic-native/fcm/ngx';

import { NgxMaskIonicModule } from 'ngx-mask-ionic';

import { CurrencyPipe } from '@angular/common';
import { Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalNuevoComponent } from './modals/modal-nuevo/modal-nuevo.component';
import { ModalSolucionarComponent } from './modals/modal-solucionar/modal-solucionar.component';
import { ModalRecordatorioComponent } from './modals/modal-recordatorio/modal-recordatorio.component';
import { ModalDetalleComponent } from './modals/modal-detalle/modal-detalle.component';
@NgModule({
  declarations: [
    AppComponent,
    NotificationsComponent,
    DetalletipoComponent,
    ModalNuevoComponent,
    ModalSolucionarComponent,
    ModalRecordatorioComponent,
    ModalDetalleComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ImagePageModule,
    SearchFilterPageModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    //ImageModalPageModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMaskIonicModule.forRoot(),
    NgbModule.forRoot(),
    NgSelectModule
  ],
  entryComponents: [
    NotificationsComponent,
    DetalletipoComponent,
    ModalNuevoComponent,
    ModalSolucionarComponent,
    ModalRecordatorioComponent,
    ModalDetalleComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WebView,
    Camera,
    File,
    Crop,
    DatePipe,
   // EmailComposer,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirestoreSettingsToken, useValue: {}},
    LocalNotifications,
    NgSelectConfig
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
