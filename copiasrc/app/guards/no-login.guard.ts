import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

import { AngularFireAuth } from "@angular/fire/auth";
import { isNullOrUndefined } from 'util';
import { AuthService } from 'src/app/servicios/auth.service';


@Injectable({
  providedIn: 'root'
})
export class NoLoginGuard implements CanActivate {

  constructor(
    private router : Router,
    private AFauth : AngularFireAuth,
    private authservice: AuthService
    ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
   
      return this.AFauth.authState.pipe(map(auth => {

        if(isNullOrUndefined(auth)){
         
         return true;
        }else{
          //this.authservice.ok2(auth.uid);
          this.authservice.idusuario =auth.uid;
          this.authservice.vehisel(this.authservice.idusuario).subscribe( usu => {
            this.authservice.idvehisel = usu[0].idVehiculo;
            this.authservice.imguser = usu[0].imagen;
            localStorage.setItem('idUsuario',auth.uid);// user.user.uid);
            localStorage.setItem('idVehiculo', this.authservice.idvehisel);
            localStorage.setItem('imguser', this.authservice.imguser);
            localStorage.setItem('nomuser', usu[0].nombre + ' ' + usu[0].apellido );
            localStorage.setItem('kmActual', usu[0].kmfinal);
            console.log('Id Vehiculo:' + this.authservice.idvehisel);
            console.log('Imagen user:' + this.authservice.imguser);
            //console.log(localStorage.getItem('nomuser'));
            this.authservice.parametros={
              'nombre': usu[0].nombre + ' ' + usu[0].apellido,
              'imagen': this.authservice.imguser
            }
            this.authservice.createUser(this.authservice.parametros);
  
  
          });
         this.router.navigate(['/home-results']);
          return false;
        }
 
       }))

  }
}
