import { Component, OnInit } from '@angular/core';
import { AuthService } from "../servicios/auth.service";
import { ModalController,NavController,AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { RegistroService } from '../servicios/registro.service';
import { VehiculoService } from '../servicios/vehiculo.service';
//import { AsyncScheduler } from 'rxjs/internal/scheduler/AsyncScheduler';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AngularFireAuth } from '@angular/fire/auth';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  information:any[];  
  automaticClose = false;
  public registroList : any = [];
  public registroList2 : any = [];
  cate:any;
  local:any;
  mivehiculo:any; 
  schedule: any;
  public vehiculoList : any = [];

  logs = [{time:new Date(), title:"Ejemplo 1", text:'testing 1',autor:'Bayron Rojas',color1:"cd-timeline-icon  positive" },
  {time:new Date(), title:"Ejemplo 2", text:'testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2',autor:'Bayron Rojas',color1:"cd-timeline-icon dark" },
  {time:new Date(), title:"Ejemplo 3", text:'testing 3 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2',autor:'Bayron Rojas',color1:"cd-timeline-icon  royal"},
  {time:new Date(), title:"Ejemplo 4", text:'testing 4 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2',autor:'Bayron Rojas',color1:"cd-timeline-icon  assertive"},
  {time:new Date(), title:"Ejemplo 6", text:'testing 6 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2 testing 2',autor:'Bayron Rojas',color1:"cd-timeline-icon  energized"} ];
 

  constructor(
    private AFauth: AngularFireAuth,
    public authservice : AuthService, 
    private router : Router,
    private modal : ModalController,
    public navCtrl: NavController,
    public registroservice: RegistroService,
    public vehiculoservice: VehiculoService,
    private localNotifications: LocalNotifications,
    private alertController:AlertController
    //public schedule: AsyncScheduler
    
    ){
      console.log(this.authservice.imguser);
    }

  ngOnInit(){
    this.vehiculoservice.getVehiculos(localStorage.getItem('idUsuario')).subscribe( veh => {
      this.vehiculoList = veh;
    });
    console.log("Id Vehiculo seleccionado" + localStorage.getItem('idVehiculo'));
    this.resetHome();
    //asyncScheduler.schedule(this.task, 1000, 0);
    this.alertNotificacion();
    // Schedule delayed notification
    
    
  }

  changeVehiculo()
  {   
    localStorage.setItem('idVehiculo',this.mivehiculo);
    this.authservice.idvehisel = this.mivehiculo;
    this.resetHome();
  }
  resetHome(){
    this.registroList.length = 0;
    this.mivehiculo = localStorage.getItem('idVehiculo');
    this.registroservice.read_Registro().subscribe( reg => {     
      if(reg.length<=0){
        this.registroList.length= 0;
      } 
      else
      {
      this.registroList = reg;
      }
      /*console.log(reg);
      if(reg.length<=0){ this.registroList.length = 0; }
      reg.forEach(element => {      
        this.local = this.registroservice.getNomLocal(element.local).subscribe( loc => {           
          return loc.nombre;           
        });      
        console.log(this.cate);    
        let it = {
            id:element.id,
            categoria:element.categoria,
            fecha:element.fecha,
            nota:element.nota,
            detalle:element.detalle,
            local:element.local,
            open:element.open
          }
        console.log(it);
        this.registroList.push(it);
        
      });*/
      
    });
    //this.information = this.registroList;
    //this.registroList[0].open = true;
  }

  toggleSection(index){
    this.registroList[index].open = !this.registroList[index].open;
    if(this.automaticClose && this.registroList[index].open){
      this.registroList
      .filter((item,itemIndex)=> itemIndex!=index)
      .map(item=> item.open = false);
    }
  }

  task(state) {
    console.log(state);
    this.schedule(state + 1, 1000); // `this` references currently executing Action,
     // which we reschedule with new state and delay
    console.log("Prueba de carga");
    console.log("Prueba 2");
  }

  alertNotificacion(){
    this.localNotifications.schedule({
      text: 'Delayed ILocalNotification',
      trigger: {at: new Date(new Date().getTime() + 3600)},
      led: 'FF0000',
      sound: null
    });
  }

  async ELIMINARREGISTRO(idregistro: string) {
    const alert = await this.alertController.create({
      header: 'Confirmación',
      message: 'Deseas eliminar el registro seleccionado!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            return new Promise<any>((resolve, rejected) => {
              this.AFauth.user.subscribe(currentuser => {                
                 this.registroservice.delete_Registro(idregistro);
              });
            });

          }
        }
      ]
    });
    await alert.present();
  }
}
//hola mundo
// hola mundito esto es otra prueba 
// hola 3
// prueba 4