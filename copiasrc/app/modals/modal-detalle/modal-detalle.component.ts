import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../servicios/auth.service';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';


@Component({
  selector: 'app-modal-detalle',
  templateUrl: './modal-detalle.component.html',
  styleUrls: ['./modal-detalle.component.scss'],
})
export class ModalDetalleComponent implements OnInit {
  public datos: any = [];
  automaticClose = false;
  listDetalle$: any = [];
  titulo:any;

  constructor(
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,

  ) { }

  ngOnInit() {
    if(localStorage.getItem('opDetalle')=="VERDETALLE"){ this.titulo = "Detalle de la Tarea"; }
    if(localStorage.getItem('opDetalle')=="VERSOLUCION"){ this.titulo = "Solucion(es) de la Tarea"; }
    if(localStorage.getItem('opDetalle')=="VERRECORDATORIO"){ this.titulo = "Recordatorio de la Tarea"; }
    if(localStorage.getItem('opDetalle')=="LISTAPENDIENTES"){ this.titulo = "Tareas Pendientes"; }
    if(localStorage.getItem('opDetalle')=="VERFECHA"){ this.titulo = "Fecha de Creación de la Tarea"; }
    if(localStorage.getItem('opDetalle')=="VERREALIZADO"){ this.titulo = "Fecha de Cierre de la Tarea"; }
    this.authservice.getDetalle(localStorage.getItem('opDetalle'),localStorage.getItem('IdTask'),localStorage.getItem('idusuario')).subscribe( (data:any[])=>{
      this.listDetalle$ = JSON.parse(JSON.stringify(data));
      //TASK = JSON.parse(JSON.stringify(data));
      console.log(this.listDetalle$);
    },(error)=>{
      console.log(error);
    })

  }

}
