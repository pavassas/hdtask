import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../servicios/auth.service';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-modal-nuevo',
  templateUrl: './modal-nuevo.component.html',
  styleUrls: ['./modal-nuevo.component.scss'],
})
export class ModalNuevoComponent implements OnInit {
  selaasignar:any;
  public usuarioAsig: any = [];
  formNuevo: FormGroup;
  resultado;
  validation_messages = {
    /*'username': [
      { type: 'required', message: 'Username is required.' },
      { type: 'minlength', message: 'Username must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
      { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      { type: 'validUsername', message: 'Your username has already been taken.' }
    ],*/
    'Detalle': [
      { type: 'required', message: 'El detalle de la tarea  es requerido' }
    ],
    'Asignar': [
      { type: 'required', message: 'Seleccionar a quien se le asignara la tarea' }
    ]
  };

  constructor(
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    
  ) {
    

   }

  ngOnInit() {
    this.cargarAsignar();
    this.resetNuevo();
    //this.usuarioAsig = this.authservice.getUsuarios();//.getPeople();
  }
  cargarAsignar()
  {
    this.authservice.getUsuarios().subscribe( (usu) => {
      this.usuarioAsig = usu
    });
  }
  GUARDARTASK(value) {
    //
    console.log(value);
    let Detalle = this.authservice.nl2br(value.Detalle);
    let task = {
      Detalle: Detalle,      
      Asignar: value.Asignar,
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   "",
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'GUARDAR',
      color: '#FFFFFF'
    };

    this.authservice.executeTask(task)/*task,Detalle,value.Asignar,localStorage.getItem('idusuario'))*/
    .subscribe((newTask) => {
      this.resultado = JSON.parse(JSON.stringify(newTask));
      if(this.resultado.res)
      {
        this.authservice.error2(this.resultado.msn);
      }
      else
      {
        this.authservice.ok2(this.resultado.msn);
        let idtarea = this.resultado.idtarea;
        const datos = {
          asignada:localStorage.getItem('idusuario'),
          para: value.Asignar,
          mensaje: "Se le asigno una nueva tarea",
          estado: "0",
          tipo: "1",
          idtarea:idtarea
        }
        if(localStorage.getItem('idusuario')!=value.Asignar)
        {
          this.authservice.create_recordatorioTask(datos)
          .then(
            () => {
              this.authservice.ok2("Se registro recordatorio al usuario asignado");
            }).catch(err => {
                this.authservice.error2(err);
            });
        }

        this.activeModal.dismiss("Cerrar modal y actualizar contador");
      }
      //this.listatareas$.unshift(newTask);
    });

    /*this.authservice.create_abastecimiento(datos)
    .then(
      res => {
        this.authservice.ok2('Abastecimiento guardado correctamente');
        this.reset_registro();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });*/
    this.formNuevo = this.formBuilder.group({
      Asignar: new FormControl('', Validators.required),
      Detalle: new FormControl('', Validators.required)
    });
    
  }
  resetNuevo()
  {
    this.formNuevo = this.formBuilder.group({
      Asignar: new FormControl('', Validators.required),
      Detalle: new FormControl(null, Validators.required)
    });
  }

}
