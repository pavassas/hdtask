import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../servicios/auth.service';
import { Observable } from 'rxjs';
import { NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-modal-recordatorio',
  templateUrl: './modal-recordatorio.component.html',
  styleUrls: ['./modal-recordatorio.component.scss'],
})
export class ModalRecordatorioComponent implements OnInit {
  formRecordatorio: FormGroup;
  resultado;
  validation_messages = {
    'Solucion': [
      { type: 'required', message: 'Debes ingresar motivo de recordatorio' }
    ]
  };
  constructor(
    public authservice: AuthService,
    public formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,

  ) { }

  ngOnInit() {

    this.resetRecordatorio();
  }
  guardarRecordatorio(value) {
    //
    let Solucion = this.authservice.nl2br(value.Solucion);
    let task = {
      Id: localStorage.getItem('IdTask'),
      Detalle: "",      
      Asignar: "",
      Estado:0,
      Creacion: "",
      Cierre:"",
      Asignada: "",
      Para:"",
      Numreco:0,
      Env:"",
      Acciones:true,
      Numsol:     0,
      Solucion:   Solucion,
      Eli:        false,
      Fer:        false,
      idusuario: localStorage.getItem('idusuario'),
      opcion: 'ENVIARRECORDATORIO',
      color: '#FFFFFF'
    };

    this.authservice.executeTask(task)/*task,Detalle,value.Asignar,localStorage.getItem('idusuario'))*/
    .subscribe((newTask) => {
      this.resultado = JSON.parse(JSON.stringify(newTask));
      if(this.resultado.res)
      {
        this.authservice.error2(this.resultado.msn);
      }
      else
      {
        this.authservice.ok2(this.resultado.msn);
        this.activeModal.dismiss("Cerrar modal y actualizar contador");
        let idtarea = this.resultado.idtarea;
        let para =  this.resultado.para;
        let asi =  this.resultado.asignada;
        const datos = {
          asignada:asi,
          para: para,
          mensaje: "Se inserto recordatorio tarea N°"+ idtarea,
          estado: "0",
          tipo: "4",
          idtarea:idtarea
        }
        if(localStorage.getItem('idusuario')!=para)
        {
          this.authservice.create_recordatorioTask(datos)
          .then(
            () => {
              this.authservice.ok2("Se registro recordatorio de tarea");
            }).catch(err => {
                this.authservice.error2(err);
            });
        }
      }
      //this.listatareas$.unshift(newTask);
    });

    /*this.authservice.create_abastecimiento(datos)
    .then(
      res => {
        this.authservice.ok2('Abastecimiento guardado correctamente');
        this.reset_registro();
      }
    ).catch(err => {
      this.authservice.error2(err);
    });*/
    this.formRecordatorio = this.formBuilder.group({
      Solucion: new FormControl('', Validators.required),
    });
    
  }
  resetRecordatorio()
  {
    this.formRecordatorio = this.formBuilder.group({
      Solucion: new FormControl('', Validators.required)
    });
  }

}
