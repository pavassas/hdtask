// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  
  apiKey: "AIzaSyD_Q4TRBtSHl1r2a8bq-19um-r1owLvpbI",
  authDomain: "hdtask-d43fb.firebaseapp.com",
  databaseURL: "https://hdtask-d43fb.firebaseio.com",
  projectId: "hdtask-d43fb",
  storageBucket: "hdtask-d43fb.appspot.com",
  messagingSenderId: "289462179527",
  appId: "1:289462179527:android:eee2d5d0dff97fbf9a50e2"
  
  /*
  apiKey: "AIzaSyAJ9ljEVVqrkSZV6jSuplU3d47ERZoppeI",
  authDomain: "charonline-71363.firebaseapp.com",
  databaseURL: "https://charonline-71363.firebaseio.com",
  projectId: "charonline-71363",
  storageBucket: "charonline-71363.appspot.com",
  messagingSenderId: "636938494167",
  appId: "1:636938494167:web:45e01fed816820e5"
  */
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
